package assignment3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.vecmath.Point3f;

import assignment2.HashOctree;
import assignment2.HashOctreeCell;
import assignment2.HashOctreeVertex;
import assignment3.marchingCubes.MCTable;
import assignment3.marchingCubes.MarchableCube;
import meshes.Point2i;
import meshes.WireframeMesh;

/**
 * You can use this sceletton - or completely ignore it and implement the
 * marching cubes algorithm from scratch.
 * 
 * @author bertholet
 *
 */
public class MarchingCubes {

	// the reconstructed surface
	public WireframeMesh result;

	// the tree to march
	private HashOctree tree;
	// per marchable cube values
	private ArrayList<Float> val;

	private HashMap<Point2i, Integer> vertexMap;

	/**
	 * Implementation of the marching cube algorithm. pass the tree and either
	 * the primary values associated to the trees edges
	 * 
	 * @param tree
	 * @param byLeaf
	 */
	public MarchingCubes(HashOctree tree) {
		this.tree = tree;
	}

	/**
	 * Perform primary Marching cubes on the tree.
	 */
	public void primaryMC(ArrayList<Float> byVertex) {
		val = byVertex;
		result = new WireframeMesh();
		vertexMap = new HashMap<Point2i, Integer>();

		for (HashOctreeCell leaf : tree.getLeafs()) {
			pushCube(leaf);
		}

	}

	/**
	 * Perform dual marchingCubes on the tree
	 */
	public void dualMC(ArrayList<Float> byVertex) {
		result = new WireframeMesh();
		vertexMap = new HashMap<Point2i, Integer>();

		float[] values = new float[tree.numberOfLeafs()];
		for (HashOctreeCell cell : tree.getLeafs()) {
			float average = 0;
			for (int i = 0b000; i <= 0b111; i++) {
				average += byVertex.get(cell.getCornerElement(i, tree).getIndex());
			}
			average /= 8;
			values[cell.getIndex()] = average;
		}

		val = new ArrayList<Float>();
		for (int i = 0; i < values.length; i++) {
			val.add(values[i]);
		}

		for (HashOctreeVertex leaf : tree.getVertices()) {
			if (!tree.isOnBoundary(leaf))
				pushCube(leaf);
		}

	}

	/**
	 * March a single cube: compute the triangles and add them to the wireframe
	 * model
	 */
	private void pushCube(MarchableCube cube) {

		float[] values = new float[8];
		values[0] = val.get(cube.getCornerElement(0b000, tree).getIndex());
		values[1] = val.get(cube.getCornerElement(0b001, tree).getIndex());
		values[2] = val.get(cube.getCornerElement(0b010, tree).getIndex());
		values[3] = val.get(cube.getCornerElement(0b011, tree).getIndex());
		values[4] = val.get(cube.getCornerElement(0b100, tree).getIndex());
		values[5] = val.get(cube.getCornerElement(0b101, tree).getIndex());
		values[6] = val.get(cube.getCornerElement(0b110, tree).getIndex());
		values[7] = val.get(cube.getCornerElement(0b111, tree).getIndex());

		Point2i[] triangles = new Point2i[15];
		initPoint2iArray(triangles);
		MCTable.resolve(values, triangles);

		int count = 0;
		Point2i[] edges = new Point2i[3];
		for (int i = 0; i < triangles.length; i++) {
			if (triangles[i].x == -1)
				continue;

			edges[count++] = triangles[i];

			if (count == 3) {
				createTriangleOnEdges(cube, edges, values);
				count = 0;
			}

		}
	}

	private void createTriangleOnEdges(MarchableCube cube, Point2i[] edges, float[] cornerValues) {
		assert cornerValues.length == 8;
		assert edges.length == 3;

		int[] indices = new int[3];

		for (int i = 0; i < 3; i++) {

			Point2i key = compute_key(cube, edges[i]);

			if (!vertexMap.containsKey(key)) {

				Point3f posA = cube.getCornerElement(edges[i].x, tree).getPosition();
				Point3f posB = cube.getCornerElement(edges[i].y, tree).getPosition();
				float a = cornerValues[edges[i].x];
				float b = cornerValues[edges[i].y];
				Point3f pos = linearInterpolation(posA, a, posB, b);

				result.vertices.add(pos);
				vertexMap.put(key, result.vertices.size() - 1);
			}

			indices[i] = vertexMap.get(key);
		}

		if (!isTriangleDegenerated(indices)) {
			result.faces.add(indices);
		}
	}

	/**
	 * Get a nicely marched wireframe mesh...
	 * 
	 * @return
	 */
	public WireframeMesh getResult() {
		return this.result;
	}

	/**
	 * compute a key value
	 * 
	 * @param n
	 * @param e
	 * @return
	 */
	private Point2i compute_key(MarchableCube n, Point2i e) {
		Point2i p = new Point2i(n.getCornerElement(e.x, tree).getIndex(), n.getCornerElement(e.y, tree).getIndex());
		if (p.x > p.y) {
			int temp = p.x;
			p.x = p.y;
			p.y = temp;
		}
		return p;
	}

	private void initPoint2iArray(Point2i[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = new Point2i(0, 0);
		}
	}

	private Point3f linearInterpolation(Point3f posA, float a, Point3f posB, float b) {
		Point3f pos = new Point3f();
		pos.scaleAdd((1 - a / (a - b)), posA, pos);
		pos.scaleAdd(a / (a - b), posB, pos);
		return pos;
	}

	private boolean isTriangleDegenerated(int[] indices) {
		int a = indices[0];
		int b = indices[1];
		int c = indices[2];
		return a == b || a == c || b == c;
	}

}
