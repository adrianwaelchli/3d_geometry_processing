package assignment3;

import static org.junit.Assert.assertEquals;
import glWrapper.GLHashtree;
import glWrapper.GLHashtree_Vertices;
import glWrapper.GLWireframeMesh;

import java.io.IOException;
import java.util.ArrayList;

import meshes.PointCloud;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;
import openGL.MyDisplay;
import sparse.CSRMatrix;
import sparse.LinearSystem;
import sparse.solver.LSQRSolver;
import assignment2.HashOctree;
import assignment2.HashOctreeVertex;

public class Assignment3_2 {

	public static void main(String[] args) {

		PointCloud pc;
		try {
			pc = ObjReader.readAsPointCloud("./objs/teapot.obj", true);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		HashOctree tree = new HashOctree(pc, 15, 1, 1.2f);

		testD0Term(tree, pc);
		testD1Term(tree, pc);
		testRegularizationTerm(tree, pc);

		tree.refineTree(10);
		float lambda0 = 1;
		float lambda1 = 0.0001f;
		float lambda2 = 10;

		LinearSystem system = SSDMatrices.ssdSystem(tree, pc, lambda0, lambda1, lambda2);
		LSQRSolver solver = new LSQRSolver();
		ArrayList<Float> perVertexValues = new ArrayList<Float>();
		solver.solve(system, perVertexValues);
		MyDisplay display = new MyDisplay();
		// Visualization of the per vertex values (blue = negative, red =
		// positive, green = 0).
		GLHashtree_Vertices gl_v = new GLHashtree_Vertices(tree);
		gl_v.addFunctionValues(perVertexValues);
		gl_v.configurePreferredShader("shaders/func.vert", "shaders/func.frag", null);
		display.addToDisplay(gl_v);

		// Discrete approximation of the zero level set: render all tree cubes
		// that have negative values.
		GLHashtree gltree = new GLHashtree(tree);
		gltree.addFunctionValues(perVertexValues);
		gltree.configurePreferredShader("shaders/octree_zro.vert", "shaders/octree_zro.frag", "shaders/octree_zro.geom");
		display.addToDisplay(gltree);

		/*
		 * Marching Cubes
		 */
		MarchingCubes marching = new MarchingCubes(tree);
		marching.dualMC(perVertexValues);
		WireframeMesh d = marching.getResult();

		GLWireframeMesh dual = new GLWireframeMesh(d);

		dual.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(dual);

	}

	private static void testD0Term(HashOctree tree, PointCloud cloud) {
		/*
		 * D0 * cellvertexcoordinates = pointCoordinates
		 */
		CSRMatrix d0 = SSDMatrices.D0Term(tree, cloud);

		ArrayList<Float> resultX = new ArrayList<Float>();
		ArrayList<Float> resultY = new ArrayList<Float>();
		ArrayList<Float> resultZ = new ArrayList<Float>();
		ArrayList<Float> cellVertexCoordsX = new ArrayList<Float>();
		ArrayList<Float> cellVertexCoordsY = new ArrayList<Float>();
		ArrayList<Float> cellVertexCoordsZ = new ArrayList<Float>();

		for (HashOctreeVertex vertex : tree.getVertices()) {
			cellVertexCoordsX.add(vertex.position.x);
			cellVertexCoordsY.add(vertex.position.y);
			cellVertexCoordsZ.add(vertex.position.z);
		}

		d0.mult(cellVertexCoordsX, resultX);
		d0.mult(cellVertexCoordsY, resultY);
		d0.mult(cellVertexCoordsZ, resultZ);

		assertEquals(d0.nRows(), cloud.numberOfPoints());
		for (int i = 0; i < cloud.numberOfPoints(); i++) {
			assertEquals(cloud.points.get(i).x, (float) resultX.get(i), 0.0001);
			assertEquals(cloud.points.get(i).y, (float) resultY.get(i), 0.0001);
			assertEquals(cloud.points.get(i).z, (float) resultZ.get(i), 0.0001);
		}
	}

	private static void testD1Term(HashOctree tree, PointCloud cloud) {
		/*
		 * If f is linear function ax + by + cz, then D1 * f = (a, b, c, a, b,
		 * c, ...).
		 */
		float a = 2, b = 3, c = 1;

		CSRMatrix d1 = SSDMatrices.D1Term(tree, cloud);

		ArrayList<Float> f = new ArrayList<Float>();
		for (HashOctreeVertex vertex : tree.getVertices()) {
			float x = vertex.position.x;
			float y = vertex.position.y;
			float z = vertex.position.z;
			f.add(a * x + b * y + c * z);
		}

		ArrayList<Float> result = new ArrayList<Float>();
		d1.mult(f, result);

		int row = 0;
		while (row < d1.nRows()) {
			assertEquals(a, result.get(row++), 0.0001);
			assertEquals(b, result.get(row++), 0.0001);
			assertEquals(c, result.get(row++), 0.0001);
		}
	}

	private static void testRegularizationTerm(HashOctree tree, PointCloud cloud) {
		/*
		 * If f is linear function ax + by + cz, then R * f = 0.
		 */
		float a = 2, b = 3, c = 1;

		CSRMatrix r = SSDMatrices.RegularizationTerm(tree);

		ArrayList<Float> f = new ArrayList<Float>();
		for (HashOctreeVertex vertex : tree.getVertices()) {
			float x = vertex.position.x;
			float y = vertex.position.y;
			float z = vertex.position.z;
			f.add(a * x + b * y + c * z);
		}

		ArrayList<Float> result = new ArrayList<Float>();
		r.mult(f, result);

		int row = 0;
		while (row < r.nRows()) {
			assertEquals(0, result.get(row++), 0.0001);
		}
	}

}
