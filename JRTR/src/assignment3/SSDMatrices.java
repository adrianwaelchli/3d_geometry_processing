package assignment3;

import java.util.ArrayList;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import assignment2.HashOctree;
import assignment2.HashOctreeCell;
import assignment2.HashOctreeVertex;
import assignment2.MortonCodes;
import meshes.PointCloud;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;
import sparse.LinearSystem;

public class SSDMatrices {

	/**
	 * Example Matrix creation: Create an identity matrix, clamped to the
	 * requested format.
	 * 
	 * @param nRows
	 * @param nCols
	 * @return
	 */
	public static CSRMatrix eye(int nRows, int nCols) {
		CSRMatrix eye = new CSRMatrix(0, nCols);

		// initialize the identity matrix part
		for (int i = 0; i < Math.min(nRows, nCols); i++) {
			eye.appendRow();
			eye.lastRow().add(
			// column i, vlue 1
					new col_val(i, 1));
		}
		// fill up the matrix with empty rows.
		for (int i = Math.min(nRows, nCols); i < nRows; i++) {
			eye.appendRow();
		}

		return eye;
	}

	/**
	 * Example matrix creation. Identity matrix restricted to boundary per
	 * vertex values.
	 * 
	 * @param tree
	 * @return
	 */
	public static CSRMatrix Eye_octree_boundary(HashOctree tree) {

		CSRMatrix result = new CSRMatrix(0, tree.numberofVertices());

		for (HashOctreeVertex v : tree.getVertices()) {
			if (MortonCodes.isVertexOnBoundary(v.code, tree.getDepth())) {
				result.appendRow();
				result.lastRow().add(new col_val(v.index, 1));
			}
		}

		return result;
	}

	/**
	 * One line per point, One column per vertex, enforcing that the
	 * interpolation of the Octree vertex values is zero at the point position.
	 * 
	 * @author Alf
	 *
	 */
	public static CSRMatrix D0Term(HashOctree tree, PointCloud cloud) {

		CSRMatrix d0 = new CSRMatrix(cloud.numberOfPoints(), tree.numberofVertices());

		int rowIdx = 0;
		for (Point3f point : cloud.points) {

			HashOctreeCell cell = tree.getCell(point);
			if (cell == null) {
				// Skip points outside the volume.
				continue;
			}
			int[] indices = getVertexIndices(cell, tree);
			float[] weights = interpolationWeights(tree, cell, point);

			fillRow(d0, rowIdx++, indices, weights);
		}

		return d0;
	}

	/**
	 * matrix with three rows per point and 1 column per octree vertex. rows
	 * with i%3 = 0 cover x gradients, =1 y-gradients, =2 z gradients; The row
	 * i, i+1, i+2 correxponds to the point/normal i/3. Three consecutant rows
	 * belong to the same gradient, the gradient in the cell of
	 * pointcloud.point[row/3];
	 * 
	 * @param tree
	 * @param cloud
	 * @return
	 */
	public static CSRMatrix D1Term(HashOctree tree, PointCloud cloud) {

		CSRMatrix d1 = new CSRMatrix(cloud.numberOfPoints() * 3, tree.numberofVertices());

		int rowIdx = 0;
		for (Point3f point : cloud.points) {

			HashOctreeCell cell = tree.getCell(point);
			if (cell == null) {
				// Skip points outside the volume.
				continue;
			}

			int[] indices = getVertexIndices(cell, tree);

			float s = 1 / (4 * cell.side);
			float[] values_x = { -s, -s, -s, -s, s, s, s, s };
			float[] values_y = { -s, -s, s, s, -s, -s, s, s };
			float[] values_z = { -s, s, -s, s, -s, s, -s, s };

			fillRow(d1, rowIdx++, indices, values_x);
			fillRow(d1, rowIdx++, indices, values_y);
			fillRow(d1, rowIdx++, indices, values_z);
		}

		return d1;
	}

	/**
	 * Regularization Term as described on the Exercise Slides.
	 * 
	 * @param tree
	 * @return
	 */
	public static CSRMatrix RegularizationTerm(HashOctree tree) {

		CSRMatrix r = new CSRMatrix(0, tree.numberofVertices());
		float sumDistProd = 0;

		for (HashOctreeVertex vertex : tree.getVertices()) {
			ArrayList<int[]> triplets = getNeighborTriplets(vertex, tree);
			for (int[] triplet : triplets) {

				HashOctreeVertex vI = tree.getVertexbyIndex(triplet[0]);
				HashOctreeVertex vJ = tree.getVertexbyIndex(triplet[1]);
				HashOctreeVertex vK = tree.getVertexbyIndex(triplet[2]);

				assert vJ == vertex;

				float distIJ = vI.position.distance(vJ.position);
				float distJK = vJ.position.distance(vK.position);
				float total = distIJ + distJK;

				sumDistProd += distIJ * distJK;

				float[] values = { 1, -distIJ / total, -distJK / total };
				int[] cols = { vJ.getIndex(), vK.getIndex(), vI.getIndex() };

				r.appendRow();
				fillRow(r, r.nRows() - 1, cols, values);
			}
		}

		r.scale(1 / sumDistProd);

		return r;
	}

	/**
	 * Set up the linear system for ssd: append the three matrices,
	 * appropriately scaled. And set up the appropriate right hand side, i.e.
	 * the b in Ax = b
	 * 
	 * @param tree
	 * @param pc
	 * @param lambda0
	 * @param lambda1
	 * @param lambda2
	 * @return
	 */
	public static LinearSystem ssdSystem(HashOctree tree, PointCloud pc, float lambda0, float lambda1, float lambda2) {

		CSRMatrix d0 = D0Term(tree, pc);
		CSRMatrix d1 = D1Term(tree, pc);
		CSRMatrix r = RegularizationTerm(tree);

		CSRMatrix matrix = new CSRMatrix(0, tree.numberofVertices());
		matrix.append(d0, (float) Math.sqrt(lambda0 / pc.numberOfPoints()));
		matrix.append(d1, (float) Math.sqrt(lambda1 / pc.numberOfPoints()));
		matrix.append(r, (float) Math.sqrt(lambda2));

		ArrayList<Float> b = new ArrayList<Float>(matrix.nRows());

		for (int i = 0; i < d0.nRows(); i++) {
			b.add(0f);
		}

		for (int i = 0; i < pc.numberOfPoints(); i++) {
			Vector3f normal = new Vector3f(pc.normals.get(i));
			normal.scale((float) Math.sqrt(lambda1 / pc.numberOfPoints()));
			b.add(normal.x);
			b.add(normal.y);
			b.add(normal.z);
		}

		for (int i = 0; i < r.nRows(); i++) {
			b.add(0f);
		}

		LinearSystem system = new LinearSystem();
		system.mat = matrix;
		system.b = b;
		return system;
	}

	private static float[] interpolationWeights(HashOctree tree, HashOctreeCell cell, Point3f point) {
		Point3f p000 = cell.getCornerElement(0b000, tree).getPosition();
		Point3f p001 = cell.getCornerElement(0b001, tree).getPosition();
		Point3f p010 = cell.getCornerElement(0b010, tree).getPosition();
		Point3f p100 = cell.getCornerElement(0b100, tree).getPosition();

		float x_d = (point.x - p000.x) / (p100.x - p000.x);
		float y_d = (point.y - p000.y) / (p010.y - p000.y);
		float z_d = (point.z - p000.z) / (p001.z - p000.z);

		float w000 = (1 - x_d) * (1 - y_d) * (1 - z_d);
		float w001 = (1 - x_d) * (1 - y_d) * z_d;
		float w010 = (1 - x_d) * y_d * (1 - z_d);
		float w011 = (1 - x_d) * y_d * z_d;
		float w100 = x_d * (1 - y_d) * (1 - z_d);
		float w101 = x_d * (1 - y_d) * z_d;
		float w110 = x_d * y_d * (1 - z_d);
		float w111 = x_d * y_d * z_d;

		return new float[] { w000, w001, w010, w011, w100, w101, w110, w111 };
	}

	private static int[] getVertexIndices(HashOctreeCell cell, HashOctree tree) {
		int[] vertexIndices = new int[8];
		for (int i = 0b000; i <= 0b111; i++) {
			vertexIndices[i] = tree.getNbr_c2v(cell, i).getIndex();
		}
		return vertexIndices;
	}

	private static void fillRow(CSRMatrix matrix, int row, int[] cols, float[] values) {
		assert cols.length == values.length;

		for (int i = 0; i < cols.length; i++) {
			matrix.set(row, cols[i], values[i]);
		}
	}

	private static ArrayList<int[]> getNeighborTriplets(HashOctreeVertex vertex, HashOctree tree) {

		ArrayList<int[]> triplets = new ArrayList<int[]>();

		int[] directions = { 0b001, 0b010, 0b100 };

		for (int i = 0; i < directions.length; i++) {
			int direction = directions[i];
			HashOctreeVertex nPlus = tree.getNbr_v2v(vertex, direction);
			HashOctreeVertex nMinus = tree.getNbr_v2vMinus(vertex, direction);

			if (nPlus != null && nMinus != null) {
				int[] triplet = { nMinus.getIndex(), vertex.getIndex(), nPlus.getIndex() };
				triplets.add(triplet);
			}
		}

		return triplets;
	}
}
