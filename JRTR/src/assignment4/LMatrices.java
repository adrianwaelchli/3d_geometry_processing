package assignment4;

import java.util.ArrayList;
import java.util.Iterator;

import javax.vecmath.Vector3f;

import meshes.HalfEdge;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import sparse.CSRMatrix;

/**
 * Methods to create different flavours of the cotangent and uniform laplacian.
 * 
 * @author Alf
 *
 */
public class LMatrices {

	private final static float cotangentClamp = 0.01F;

	/**
	 * The uniform Laplacian
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix uniformLaplacian(HalfEdgeStructure hs) {
		int n = hs.getVertices().size();
		CSRMatrix laplacian = new CSRMatrix(n, n);

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {

			Vertex vertex = vertexIterator.next();
			laplacian.set(vertex.index, vertex.index, -1);

			float valence = vertex.valence();

			Iterator<Vertex> neighborhood = vertex.iteratorVV();
			while (neighborhood.hasNext()) {
				Vertex neighbor = neighborhood.next();
				laplacian.set(vertex.index, neighbor.index, 1 / valence);
			}
		}

		return laplacian;
	}

	/**
	 * The cotangent Laplacian
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix mixedCotanLaplacian(HalfEdgeStructure hs) {
		int n = hs.getVertices().size();
		CSRMatrix laplacian = new CSRMatrix(n, n);

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();

			float mixedArea = vertex.mixedArea();

			Iterator<HalfEdge> edgeIterator = vertex.iteratorVE();
			while (edgeIterator.hasNext()) {
				HalfEdge edge = edgeIterator.next();

				float alpha = edge.alpha();
				float beta = edge.beta();
				float cotAlpha = (float) (1 / Math.tan(alpha));
				float cotBeta = (float) (1 / Math.tan(beta));

				cotAlpha = clampAbsolute(cotAlpha, cotangentClamp);
				cotBeta = clampAbsolute(cotBeta, cotangentClamp);

				float weight = 0.5F * (cotAlpha + cotBeta) / mixedArea;

				laplacian.set(vertex.index, edge.end().index, weight);
				laplacian.set(vertex.index, vertex.index, laplacian.get(vertex.index, vertex.index) - weight);
			}
		}

		return laplacian;
	}

	/**
	 * A symmetric cotangent Laplacian, cf Assignment 4, exercise 4.
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix symmetricCotanLaplacian(HalfEdgeStructure hs) {
		int n = hs.getVertices().size();
		CSRMatrix laplacian = new CSRMatrix(n, n);

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();

			float mixedArea1 = vertex.mixedArea();

			Iterator<HalfEdge> edgeIterator = vertex.iteratorVE();
			while (edgeIterator.hasNext()) {
				HalfEdge edge = edgeIterator.next();

				float mixedArea2 = edge.end().mixedArea();

				float alpha = edge.alpha();
				float beta = edge.beta();
				float cotAlpha = (float) (1 / Math.tan(alpha));
				float cotBeta = (float) (1 / Math.tan(beta));

				cotAlpha = clampAbsolute(cotAlpha, cotangentClamp);
				cotBeta = clampAbsolute(cotBeta, cotangentClamp);

				float weight = (float) ((cotAlpha + cotBeta) / Math.sqrt(mixedArea1 * mixedArea2));

				laplacian.set(vertex.index, edge.end().index, weight);
				laplacian.set(vertex.index, vertex.index, laplacian.get(vertex.index, vertex.index) - weight);
			}
		}

		return laplacian;
	}

	/**
	 * The unnormalized cotangent laplacian (not devided by any areas).
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix unnormalizedCotanLaplacian(HalfEdgeStructure hs) {
		int n = hs.getVertices().size();
		CSRMatrix laplacian = new CSRMatrix(n, n);

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {

			Vertex vertex = vertexIterator.next();
			if (vertex.isOnBorder()) {
				continue;
			}
			Iterator<HalfEdge> edgeIterator = vertex.iteratorVE();

			while (edgeIterator.hasNext()) {
				HalfEdge edge = edgeIterator.next();

				float alpha = edge.alpha();
				float beta = edge.beta();
				float cotAlpha = (float) (1 / Math.tan(alpha));
				float cotBeta = (float) (1 / Math.tan(beta));

				cotAlpha = clampAbsolute(cotAlpha, cotangentClamp);
				cotBeta = clampAbsolute(cotBeta, cotangentClamp);
				
				float weight = 0.5F * (cotAlpha + cotBeta);

				laplacian.set(vertex.index, edge.end().index, weight);
				laplacian.set(vertex.index, vertex.index, laplacian.get(vertex.index, vertex.index) - weight);
			}
		}

		return laplacian;
	}

	public static void mult(CSRMatrix m, HalfEdgeStructure s, ArrayList<Vector3f> res) {
		ArrayList<Float> x = new ArrayList<>(), b = new ArrayList<>(s.getVertices().size());
		x.ensureCapacity(s.getVertices().size());

		res.clear();
		res.ensureCapacity(s.getVertices().size());
		for (Vertex v : s.getVertices()) {
			x.add(0.f);
			res.add(new Vector3f());
		}

		for (int i = 0; i < 3; i++) {

			// setup x
			for (Vertex v : s.getVertices()) {
				switch (i) {
				case 0:
					x.set(v.index, v.getPos().x);
					break;
				case 1:
					x.set(v.index, v.getPos().y);
					break;
				case 2:
					x.set(v.index, v.getPos().z);
					break;
				}

			}

			m.mult(x, b);

			for (Vertex v : s.getVertices()) {
				switch (i) {
				case 0:
					res.get(v.index).x = b.get(v.index);
					break;
				case 1:
					res.get(v.index).y = b.get(v.index);
					break;
				case 2:
					res.get(v.index).z = b.get(v.index);
					break;
				}

			}
		}
	}

	private static float clampAbsolute(float value, float clamp) {
		return Math.abs(value) < clamp ? clamp : value;
	}
}
