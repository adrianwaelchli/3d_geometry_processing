package assignment4;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;

import javax.vecmath.Vector3f;

import org.junit.Before;
import org.junit.Test;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;

/**
 * @author Adrian Waelchli
 */
public class Assignment4_Tests {

	public static final float numericInaccuracy = 0.01F;

	// A sphere of radius 2.
	private HalfEdgeStructure hs;
	// An ugly sphere of radius 1, don't expect the Laplacians
	// to perform accurately on this mesh.
	private HalfEdgeStructure hs2;

	@Before
	public void setUp() {
		try {
			WireframeMesh m = ObjReader.read("objs/sphere.obj", false);
			hs = new HalfEdgeStructure();
			hs.init(m);

			m = ObjReader.read("objs/uglySphere.obj", false);
			hs2 = new HalfEdgeStructure();
			hs2.init(m);

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

	@Test
	public void testUniformLaplacian() {
		CSRMatrix uniformLaplacion = LMatrices.uniformLaplacian(hs);
		assertRowSumsZero(uniformLaplacion);

		/*
		 * If per-row normalization by number of neighbors is dropped, the
		 * laplacian should be symmetric.
		 */
		CSRMatrix unNormalized = new CSRMatrix(uniformLaplacion.nRows(), uniformLaplacion.nCols());
		Iterator<Vertex> vertexIterator = hs.iteratorV();

		while (vertexIterator.hasNext()) {

			Vertex vertex = vertexIterator.next();
			ArrayList<col_val> row = uniformLaplacion.getRow(vertex.index);

			float valence = vertex.valence();

			for (col_val element : row) {
				unNormalized.set(vertex.index, element.col, uniformLaplacion.get(vertex.index, element.col) * valence);
			}
		}

		assertSymmetricMatrix(unNormalized);
	}

	@Test
	public void testMixedCotanLaplacian() {
		CSRMatrix cotangentLaplacian = LMatrices.mixedCotanLaplacian(hs);
		assertRowSumsZero(cotangentLaplacian);

		/*
		 * If per-row normalization by mixedArea is dropped, the laplacian
		 * should be symmetric.
		 */
		CSRMatrix unNormalized = new CSRMatrix(cotangentLaplacian.nRows(), cotangentLaplacian.nCols());
		Iterator<Vertex> vertexIterator = hs.iteratorV();

		while (vertexIterator.hasNext()) {

			Vertex vertex = vertexIterator.next();
			ArrayList<col_val> row = cotangentLaplacian.getRow(vertex.index);

			float mixedArea = vertex.mixedArea();

			for (col_val element : row) {
				unNormalized.set(vertex.index, element.col, cotangentLaplacian.get(vertex.index, element.col) * mixedArea);
			}
		}

		assertSymmetricMatrix(unNormalized);

		/*
		 * The mean curvature of a sphere of radius r should be equal to 1 / r.
		 */
		ArrayList<Vector3f> mCurvNormals = new ArrayList<Vector3f>();
		LMatrices.mult(cotangentLaplacian, hs, mCurvNormals);

		for (Vector3f mCurvNormal : mCurvNormals) {
			float meanCurvature = mCurvNormal.length() / 2;
			assertEquals(0.5F, meanCurvature, numericInaccuracy);
		}

		/*
		 * The mean curvature normal should be normal to the sphere.
		 */
		vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();

			Vector3f toVertex = new Vector3f(vertex.getPos());
			Vector3f mCurvNormal = mCurvNormals.get(vertex.index);
			mCurvNormal.negate(); // laplace = -2Hn
			assertEquals(0, toVertex.angle(mCurvNormal), numericInaccuracy);
		}
	}

	@Test
	public void testSymmetricCotanLaplacian() {
		CSRMatrix laplacian = LMatrices.symmetricCotanLaplacian(hs);
		assertRowSumsZero(laplacian);
		assertSymmetricMatrix(laplacian);
	}

	private void assertRowSumsZero(CSRMatrix matrix) {
		/*
		 * Row sums should equal zero
		 */
		for (int i = 0; i < matrix.nRows(); i++) {
			ArrayList<col_val> row = matrix.getRow(i);
			float sum = 0;
			for (col_val element : row) {
				sum += element.val;
			}
			assertEquals(0, sum, numericInaccuracy);
		}
	}

	private void assertSymmetricMatrix(CSRMatrix matrix) {
		assertEquals(matrix.nRows(), matrix.nCols());

		for (int i = 0; i < matrix.nRows(); i++) {
			for (int j = 0; j < matrix.nCols(); j++) {
				assertEquals(matrix.get(i, j), matrix.get(j, i), numericInaccuracy);
			}
		}
	}

}
