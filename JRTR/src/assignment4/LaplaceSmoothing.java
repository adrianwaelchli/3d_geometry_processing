package assignment4;

import java.util.ArrayList;
import java.util.Iterator;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import sparse.CSRMatrix;
import sparse.solver.Solver;

/**
 * @author Adrian Waelchli
 */
public class LaplaceSmoothing {

	private final HalfEdgeStructure hs;
	private CSRMatrix laplacian;

	public LaplaceSmoothing(HalfEdgeStructure hs) {
		this.hs = hs;
		useMixedCotanLaplacian();
	}

	public LaplaceSmoothing(HalfEdgeStructure hs, CSRMatrix laplacian) {
		this.hs = hs;
		useLaplacian(laplacian);
	}

	public void useUniformLaplacian() {
		this.laplacian = LMatrices.uniformLaplacian(hs);
	}

	public void useMixedCotanLaplacian() {
		this.laplacian = LMatrices.mixedCotanLaplacian(hs);
	}

	public void useLaplacian(CSRMatrix laplacian) {
		assert laplacian.nRows() == laplacian.nCols() && laplacian.nRows() == hs.getVertices().size();
		this.laplacian = laplacian;
	}

	public void smoothen(Solver solver, float lambda) {

		laplacian.scale(-lambda);
		CSRMatrix mat = new CSRMatrix(laplacian.nCols(), laplacian.nRows());
		mat.add(laplacian, identityMatrix(laplacian.nCols()));

		/*
		 * Construct right-hand side for each coordinate.
		 */
		ArrayList<Float> pointsX = initZero(mat.nRows());
		ArrayList<Float> pointsY = initZero(mat.nRows());
		ArrayList<Float> pointsZ = initZero(mat.nRows());

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();
			pointsX.set(vertex.index, vertex.getPos().x);
			pointsY.set(vertex.index, vertex.getPos().y);
			pointsZ.set(vertex.index, vertex.getPos().z);
		}

		/*
		 * Solve for each coordinate separately.
		 */
		ArrayList<Float> solutionX = initZero(mat.nRows());
		ArrayList<Float> solutionY = initZero(mat.nRows());
		ArrayList<Float> solutionZ = initZero(mat.nRows());

		solver.solve(mat, pointsX, solutionX);
		solver.solve(mat, pointsY, solutionY);
		solver.solve(mat, pointsZ, solutionZ);

		float originalVolume = hs.volume();

		Iterator<Vertex> iterator = hs.iteratorV();
		while (iterator.hasNext()) {
			Vertex vertex = iterator.next();
			vertex.getPos().x = solutionX.get(vertex.index);
			vertex.getPos().y = solutionY.get(vertex.index);
			vertex.getPos().z = solutionZ.get(vertex.index);
		}

		float newVolume = hs.volume();
		float scale = (float) Math.pow(originalVolume / newVolume, 1.0 / 3);

		hs.scalePositions(scale);
	}

	private static CSRMatrix identityMatrix(int size) {
		CSRMatrix id = new CSRMatrix(size, size);
		for (int i = 0; i < size; i++) {
			id.set(i, i, 1);
		}
		return id;
	}

	private static ArrayList<Float> initZero(int size) {
		ArrayList<Float> list = new ArrayList<Float>();
		for (int i = 0; i < size; i++) {
			list.add(0F);
		}
		return list;
	}

}
