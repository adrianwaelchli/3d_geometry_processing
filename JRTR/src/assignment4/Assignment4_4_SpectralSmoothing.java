package assignment4;

import glWrapper.GLHalfedgeStructure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.vecmath.Vector3f;

import meshes.HEData1d;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;
import openGL.MyDisplay;
import sparse.CSRMatrix;
import sparse.SCIPYEVD;

/**
 * @author Adrian Waelchli
 */
public class Assignment4_4_SpectralSmoothing {

	private static MyDisplay display1, display2;

	public static void main(String[] args) {

		display1 = new MyDisplay();
		display2 = new MyDisplay();

		demoEigenVectorsOfSphere(20);

		demoSpectralSmoothing("objs/teapot.obj", 20);
	}

	private static void demoSpectralSmoothing(String object, int numEigen) {
		HalfEdgeStructure hs = loadAsHalfedgeStructure(object);
		
//		numEigen = hs.getVertices().size();

		CSRMatrix laplacian = LMatrices.symmetricCotanLaplacian(hs);
		ArrayList<Float> eigenValues = new ArrayList<Float>();
		ArrayList<ArrayList<Float>> eigenVectors = new ArrayList<ArrayList<Float>>();

		try {
			SCIPYEVD.doSVD(laplacian, "spectral_smoothing", numEigen, eigenValues, eigenVectors);
		} catch (IOException e) {
			e.printStackTrace();
		}

		CSRMatrix mat = new CSRMatrix(laplacian.nRows(), numEigen);
		for (int eigenIdx = 0; eigenIdx < numEigen; eigenIdx++) {
			ArrayList<Float> eigenVector = eigenVectors.get(eigenIdx);
			for (int i = 0; i < eigenVector.size(); i++) {
				mat.set(i, eigenIdx, eigenVector.get(i));
			}
		}

		CSRMatrix weights = new CSRMatrix(numEigen, numEigen);
		for (int weightIdx = 0; weightIdx < numEigen; weightIdx++) {
			float freq = (float) Math.sqrt(eigenValues.get(weightIdx));
			float function = 1f;
			weights.set(weightIdx, weightIdx, function);
		}
		CSRMatrix m = new CSRMatrix(numEigen, laplacian.nCols());
		CSRMatrix filter = new CSRMatrix(laplacian.nRows(), laplacian.nCols());

		weights.mult(mat.transposed(), m);
		mat.mult(m, filter);

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		ArrayList<Vector3f> result = new ArrayList<Vector3f>();

		LMatrices.mult(filter, hs, result);

		/*
		 * Update the positions of the mesh.
		 */
		vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();
			vertex.getPos().set(result.get(vertex.index));
		}

		GLHalfedgeStructure glSmooth = new GLHalfedgeStructure(hs);
		glSmooth.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");

		display2.addToDisplay(glSmooth);
	}

	private static void demoEigenVectorsOfSphere(int numEigen) {

		String object = "objs/sphere.obj";

		HalfEdgeStructure hs = loadAsHalfedgeStructure(object);

		CSRMatrix laplacian = LMatrices.symmetricCotanLaplacian(hs);
		ArrayList<Float> eigenValues = new ArrayList<Float>();
		ArrayList<ArrayList<Float>> eigenVectors = new ArrayList<ArrayList<Float>>();

		try {
			SCIPYEVD.doSVD(laplacian, "sphere", numEigen, eigenValues, eigenVectors);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int eigenIdx = 0; eigenIdx < numEigen; eigenIdx++) {
			displayEigenVector(eigenVectors.get(eigenIdx), eigenValues.get(eigenIdx), loadAsHalfedgeStructure(object));
		}
	}

	private static void displayEigenVector(ArrayList<Float> eigenVector, float eigenValue, HalfEdgeStructure hs) {
		/*
		 * Scale vertex positions of sphere with the absolute value of the
		 * eigenvector.
		 */
		float min = min(eigenVector);
		float max = max(eigenVector);

		float scale = 10;

		HEData1d values = new HEData1d(hs);

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();
			float v = (eigenVector.get(vertex.index) - min) / (max - min);
			values.put(vertex, v);
			vertex.getPos().scale(scale * Math.abs(eigenVector.get(vertex.index)));
		}

		GLHalfedgeStructure glEigenVector = new GLHalfedgeStructure(hs);

		glEigenVector.add(values, "eigenFunction");
		glEigenVector.configurePreferredShader("shaders/eigenFunction.vert", "shaders/eigenFunction.frag", null);

		display1.addToDisplay(glEigenVector);
	}

	private static HalfEdgeStructure loadAsHalfedgeStructure(String object) {

		HalfEdgeStructure hs = new HalfEdgeStructure();
		try {
			WireframeMesh m = ObjReader.read(object, true);
			hs.init(m);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return hs;
	}

	private static float max(ArrayList<Float> list) {
		float max = Float.NEGATIVE_INFINITY;

		for (float val : list) {
			max = val > max ? val : max;
		}

		return max;
	}

	private static float min(ArrayList<Float> list) {
		float min = Float.POSITIVE_INFINITY;

		for (float val : list) {
			min = val < min ? val : min;
		}

		return min;
	}
}
