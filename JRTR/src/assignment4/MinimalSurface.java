package assignment4;

import java.util.ArrayList;
import java.util.Iterator;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;
import sparse.solver.Solver;

/**
 * @author Adrian Waelchli
 */
public class MinimalSurface {

	private final HalfEdgeStructure hs;

	public MinimalSurface(HalfEdgeStructure hs) {
		this.hs = hs;
	}

	public void solve(Solver solver, int iterations) {
		for (int i = 0; i < iterations; i++) {
			iteration(solver);
		}
	}

	public void solve(Solver solver) {
		float areaOld = hs.area();
		iteration(solver);
		float areaNew = hs.area();

		if ((areaOld - areaNew) / areaOld > 0.1) {
			solve(solver);
		}
	}

	private void iteration(Solver solver) {

		CSRMatrix mat = LMatrices.mixedCotanLaplacian(hs);

		/*
		 * Delete cotangent weights for rows that correspond to a boundary
		 * vertex.
		 */
		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();
			if (vertex.isOnBorder()) {
				ArrayList<col_val> row = mat.getRow(vertex.index);
				row.clear();
				row.add(new col_val(vertex.index, 1));
			}
		}

		/*
		 * Right-hand side: Zero for non-boundary vertices, vertex position for
		 * boundary vertices.
		 */
		ArrayList<Float> bX = initZero(mat.nRows());
		ArrayList<Float> bY = initZero(mat.nRows());
		ArrayList<Float> bZ = initZero(mat.nRows());

		vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();

			if (vertex.isOnBorder()) {
				bX.set(vertex.index, vertex.getPos().x);
				bY.set(vertex.index, vertex.getPos().y);
				bZ.set(vertex.index, vertex.getPos().z);
			}
		}

		/*
		 * Solve the system of equations
		 */
		ArrayList<Float> solutionX = initZero(mat.nRows());
		ArrayList<Float> solutionY = initZero(mat.nRows());
		ArrayList<Float> solutionZ = initZero(mat.nRows());

		solver.solve(mat, bX, solutionX);
		solver.solve(mat, bY, solutionY);
		solver.solve(mat, bZ, solutionZ);

		/*
		 * Update the mesh with the new positions
		 */
		vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();
			vertex.getPos().x = solutionX.get(vertex.index);
			vertex.getPos().y = solutionY.get(vertex.index);
			vertex.getPos().z = solutionZ.get(vertex.index);
		}
	}

	private static ArrayList<Float> initZero(int size) {
		ArrayList<Float> list = new ArrayList<Float>();
		for (int i = 0; i < size; i++) {
			list.add(0F);
		}
		return list;
	}
}
