package assignment4;

import java.util.ArrayList;
import java.util.Iterator;

import javax.vecmath.Vector3f;

import glWrapper.GLHalfedgeStructure;
import meshes.HEData1d;
import meshes.HEData3d;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;
import openGL.MyDisplay;
import sparse.CSRMatrix;

/**
 * @author Adrian Waelchli
 */
public class Assignment4_1_Demo {

	public static void main(String[] args) {

		MyDisplay display1 = new MyDisplay();
		MyDisplay display2 = new MyDisplay();

		HalfEdgeStructure sphere = new HalfEdgeStructure();
		HalfEdgeStructure dragon = new HalfEdgeStructure();
		try {
			WireframeMesh m = ObjReader.read("objs/sphere.obj", false);
			sphere.init(m);

			m = ObjReader.read("objs/dragon.obj", true);
			dragon.init(m);

		} catch (Exception e) {
			e.printStackTrace();
		}

		CSRMatrix cotLaplacianSphere = LMatrices.mixedCotanLaplacian(sphere);
		CSRMatrix unifLaplacianSphere = LMatrices.uniformLaplacian(sphere);

		curvatureDemo(sphere, cotLaplacianSphere, display1);
		curvatureDemo(sphere, unifLaplacianSphere, display1);
		
		CSRMatrix cotLaplacianDragon = LMatrices.mixedCotanLaplacian(dragon);
		CSRMatrix unifLaplacianDragon = LMatrices.uniformLaplacian(dragon);

		curvatureDemo(dragon, cotLaplacianDragon, display2);
		curvatureDemo(dragon, unifLaplacianDragon, display2);

	}

	private static void curvatureDemo(HalfEdgeStructure hs, CSRMatrix laplacian, MyDisplay display) {

		ArrayList<Vector3f> perVertexLaplacian = new ArrayList<Vector3f>();
		LMatrices.mult(laplacian, hs, perVertexLaplacian);

		HEData3d curvatureData = new HEData3d(hs);
		HEData1d curvatureMagnitude = new HEData1d(hs);

		Iterator<Vertex> vertexIterator = hs.iteratorV();
		while (vertexIterator.hasNext()) {
			Vertex vertex = vertexIterator.next();
			Vector3f vertexLaplacian = perVertexLaplacian.get(vertex.index);
			Vector3f normal = new Vector3f(vertexLaplacian);
			normal.scale(-0.5F);

			curvatureData.put(vertex, normal);
			curvatureMagnitude.put(vertex, normal.length());
		}

		GLHalfedgeStructure glNormals = new GLHalfedgeStructure(hs);
		GLHalfedgeStructure glCurvature = new GLHalfedgeStructure(hs);

		glNormals.add(curvatureData, "curvatureNormal");
		glNormals.configurePreferredShader("shaders/curvatureNormal.vert", "shaders/curvatureNormal.frag", "shaders/curvatureNormal.geom");

		glCurvature.add(curvatureMagnitude, "meanCurvature");
		glCurvature.configurePreferredShader("shaders/meanCurvature.vert", "shaders/meanCurvature.frag", null);

		display.addToDisplay(glNormals);
		display.addToDisplay(glCurvature);

	}

}
