package assignment4;

import java.io.IOException;

import glWrapper.GLHalfedgeStructure;
import glWrapper.GLWireframeMesh;
import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;
import openGL.MyDisplay;
import sparse.solver.LSQRSolver;
import sparse.solver.Solver;

public class Assignment4_2_uMasking {

	public static void main(String[] arg) throws IOException {
		demo();

	}

	private static void demo() throws IOException {

		String object = "./objs/bunny.obj";
		MyDisplay display = new MyDisplay();

		WireframeMesh m = ObjReader.read(object, true);
		GLWireframeMesh glwf = new GLWireframeMesh(m);
		glwf.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glwf);

		HalfEdgeStructure input = loadAsHalfedgeStructure(object);
		HalfEdgeStructure smoothed = loadAsHalfedgeStructure(object);

		Solver solver = new LSQRSolver();

		LaplaceSmoothing smoothing = new LaplaceSmoothing(smoothed);
		smoothing.useMixedCotanLaplacian();
		smoothing.smoothen(solver, 0.1F);

		float s = 2F;

		/*
		 * Unsharp Masking: (input - smoothed) * s + smoothed
		 */
		smoothed.scalePositions(-1);
		input.addPositions(smoothed.getVertices());
		input.scalePositions(s);
		smoothed.scalePositions(-1);
		input.addPositions(smoothed.getVertices());

		GLHalfedgeStructure glUMasked = new GLHalfedgeStructure(input);
		glUMasked.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glUMasked);
	}

	private static HalfEdgeStructure loadAsHalfedgeStructure(String object) {

		HalfEdgeStructure hs = new HalfEdgeStructure();
		try {
			WireframeMesh m = ObjReader.read(object, true);
			hs.init(m);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return hs;
	}

}
