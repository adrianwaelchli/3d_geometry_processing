package assignment4;

import glWrapper.GLHalfedgeStructure;
import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;
import openGL.MyDisplay;
import sparse.solver.LSQRSolver;

/**
 * Smoothing
 * 
 * @author Alf
 *
 */
public class Assignment4_2_Smoothing {

	public static void main(String[] args) {

		MyDisplay display = new MyDisplay();

		String object = "objs/bunny.obj";
		HalfEdgeStructure hsOriginal = loadAsHalfedgeStructure(object);
		HalfEdgeStructure hsCotan = loadAsHalfedgeStructure(object);
		HalfEdgeStructure hsUniform = loadAsHalfedgeStructure(object);

		GLHalfedgeStructure original = new GLHalfedgeStructure(hsOriginal);
		original.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(original);

		LSQRSolver solver = new LSQRSolver();

		/*
		 * Smoothing using mixed cotangent laplacian
		 */
		LaplaceSmoothing smoother = new LaplaceSmoothing(hsCotan);
		smoother.useMixedCotanLaplacian();
		smoother.smoothen(solver, 0.1F);

		GLHalfedgeStructure glCotan = new GLHalfedgeStructure(hsCotan);
		glCotan.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glCotan);

		/*
		 * Smoothing using uniform laplacian
		 */
		smoother = new LaplaceSmoothing(hsUniform);
		smoother.useUniformLaplacian();
		smoother.smoothen(solver, 20F);

		GLHalfedgeStructure glUniform = new GLHalfedgeStructure(hsUniform);
		glUniform.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glUniform);
	}

	private static HalfEdgeStructure loadAsHalfedgeStructure(String object) {

		HalfEdgeStructure hs = new HalfEdgeStructure();
		try {
			WireframeMesh m = ObjReader.read(object, true);
			hs.init(m);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return hs;
	}

}
