package meshes;

import java.util.ArrayList;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

/**
 * A Wireframe Mesh represents a mesh as a list of vertices and a list of faces.
 * Very lightweight representation.
 * 
 * @author bertholet
 *
 */
public class WireframeMesh {

	public ArrayList<Point3f> vertices;
	public ArrayList<Point3f> colors;
	public ArrayList<int[]> faces;
	public ArrayList<Vector3f> normals;

	public WireframeMesh() {
		vertices = new ArrayList<Point3f>();
		faces = new ArrayList<>();
		colors = new ArrayList<>();
		normals = new ArrayList<>();
	}
}
