package assignment7;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import assignment6.Linalg3x3;

public class RigidAlignment {

	// for the svd.
	final static Linalg3x3 l = new Linalg3x3(10);// argument controls number of
													// iterations for ed/svd
													// decompositions
													// 3 = very low precision
													// but high speed. 3 seems
													// to be good enough

	public static Matrix4f rigidAlign(LandmarkedMesh template_, LandmarkedMesh face_) {

		assert template_.equalKeySet(face_);
		assert template_ != face_;

		List<Point3f> template = template_.getLandMarkPositions();
		List<Point3f> face = face_.getLandMarkPositions();

		assert !template.equals(face);

		// Align to centroid
		Point3f t1 = centroid(template);
		Point3f t2 = centroid(face);

		template = sub(template, t1);
		face = sub(face, t2);

		// Compute mean distance/length
		float d1 = meanLength(template);
		float d2 = meanLength(face);

		float scale = d1 / d2;

		face = scale(face, scale);

		Matrix3f rotation = rotation(face, template);

		// Combine all steps

		Matrix4f s = new Matrix4f();
		s.setIdentity();
		s.setScale(scale);

		Matrix4f t1_ = new Matrix4f();
		t1_.setIdentity();
		t1_.setTranslation(new Vector3f(t1));
		Matrix4f t2_ = new Matrix4f();
		t2_.setIdentity();
		t2.negate();
		t2_.setTranslation(new Vector3f(t2));

		Matrix4f rot = new Matrix4f();
		rot.setIdentity();
		rot.setRotation(rotation);

		Matrix4f transformation = new Matrix4f();
		transformation.setIdentity();
		transformation.mul(t1_);
		transformation.mul(rot);
		transformation.mul(s);
		transformation.mul(t2_);

		return transformation;
	}

	private static Point3f centroid(List<Point3f> points) {
		Point3f p = new Point3f(0, 0, 0);
		int n = 0;
		for (Point3f v : points) {
			p.add(v);
			n++;
		}
		p.scale(1.f / n);
		return p;
	}

	public static float meanLength(List<Point3f> points) {
		float d = 0;
		int n = points.size();
		for (Point3f point : points) {
			Vector3f v = new Vector3f(point);
			d += v.length();
		}

		d /= n;
		return d;
	}

	private static List<Point3f> sub(List<Point3f> p, Point3f q) {
		List<Point3f> v = new ArrayList<Point3f>();
		for (Point3f y : p) {
			Point3f x = new Point3f(y);
			x.sub(q);
			v.add(x);
		}
		return v;
	}

	private static List<Point3f> scale(List<Point3f> p, float scale) {
		List<Point3f> v = new ArrayList<Point3f>();
		for (Point3f y : p) {
			Point3f x = new Point3f(y);
			x.scale(scale);
			v.add(x);
		}
		return v;
	}

	public static Matrix3f rotation(List<Point3f> x, List<Point3f> y) {
		assert x.size() == y.size();
		final int n = x.size();
		final int d = 3;

		// (3) Compute the d x d covariance matrix: X^T <weights> Y
		// X and Y contain in their columns the the x and y vectors.
		// C.f. linear procrustes: A is X, B is Y, we find R st. RA = B as well
		// as possible
		// (according to frobenius norm: sum of squares of entries (and taking
		// the root in the end) -- leastsquares!)
		// aka. Kabsch algorithm
		float[][] S = new float[3][3];
		float[][] X = new float[d][n], Y = new float[d][n];

		for (int i = 0; i < d; i++)
			for (int j = 0; j < n; j++) {
				X[i][j] = comp(x.get(j), i);
				Y[i][j] = comp(y.get(j), i);
			}
		/*
		 * for (int i = 0; i < 3; i++) for (int j = 0; j < 3; j++) for (int k =
		 * 0; k < n; k++) S[i][j] += comp(x[k],i)*comp(y[k],j); // TODO check
		 */

		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				for (int k = 0; k < n; k++)
					S[i][j] += X[i][k] * Y[j][k]; // TODO check

		Matrix3f S_ = new Matrix3f(S[0][0], S[0][1], S[0][2], S[1][0], S[1][1], S[1][2], S[2][0], S[2][1], S[2][2]);

		// (4) Compute the singular value decomposition
		Matrix3f U = new Matrix3f(), sigma = new Matrix3f(), V = new Matrix3f();
		l.svd(S_, U, sigma, V);

		Matrix3f Ut = new Matrix3f(U);
		Ut.transpose();
		Matrix3f Vt = new Matrix3f(V);
		Vt.transpose();

		// check svd /
		Matrix3f U_sigma_Vt = new Matrix3f(U);
		U_sigma_Vt.mul(sigma);
		U_sigma_Vt.mul(Vt);
		// assert S_.epsilonEquals(U_sigma_Vt, 1e-4f);

		// The rotation we are looking for is then
		Matrix3f VUt = new Matrix3f(V);
		VUt.mul(Ut);
		float det = VUt.determinant();
		// assert Math.abs(Math.abs(det) - 1) < 1e-4 : det;
		Matrix3f R = new Matrix3f(V);
		R.mul(new Matrix3f(1, 0, 0, 0, 1, 0, 0, 0, det));
		R.mul(Ut);

		// Check that we got a rotation matrix
		// assert Math.abs(R.determinant() - 1) < 1e-3;
		return R;
	}

	static float comp(Point3f x, int d) {
		float[] y = new float[3];
		x.get(y);
		return y[d];
	}
}
