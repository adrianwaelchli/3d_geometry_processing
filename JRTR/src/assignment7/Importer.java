package assignment7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;

public class Importer {

	private static List<String> ignoredLabels;

	public static Map<String, Integer> readLandMarks(String filename) {

		Map<String, Integer> map = new HashMap<String, Integer>();

		Scanner scan = null;
		try {
			scan = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (scan.hasNextLine()) {
			String[] s = scan.nextLine().split(" ");
			String label;
			String index;
			if (2 == s.length) {
				label = s[1];
				index = s[0];

			} else {
				label = s[0];
				index = s[1];
			}

			if (ignoredLabels != null && ignoredLabels.contains(label)) {
				continue;
			}
			map.put(label, Integer.parseInt(index));

		}

		return map;

	}

	public static WireframeMesh readMesh(String filename) {

		WireframeMesh wf = null;
		try {

			wf = ObjReader.read(filename, false);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return wf;

	}

	public static void ignoreLabels(String file) {
		ignoredLabels = new ArrayList<String>();
		Scanner scan = null;
		try {
			scan = new Scanner(new File(file));
		} catch (FileNotFoundException e) {
			assert false : "Labels file invalid";
		}

		while (scan.hasNextLine()) {
			ignoredLabels.add(scan.nextLine());
		}
	}
	
	public static void ignoreLabel(String ... args) {
		ignoredLabels = new ArrayList<String>();
		ignoredLabels.addAll(Arrays.asList(args));
		
	}

	public static LandmarkedMesh readLandMarkedMesh(String fileMesh, String fileLandMarks) {
		return new LandmarkedMesh(readLandMarks(fileLandMarks), readMesh(fileMesh));
	}

}
