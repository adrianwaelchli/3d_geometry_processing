package assignment7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.vecmath.Point3f;

import sparse.CSRMatrix;

public class WarpingConstraints {

	private Map<Integer, Point3f> constraints;
	private Map<Integer, Float> weights;
	private final int n;

	public int count() {
		return constraints.size();
	}
	
	public WarpingConstraints(int n) {
		super();
		this.constraints = new HashMap<Integer, Point3f>();
		this.n = n;
		weights = new HashMap<Integer, Float>();
	}

	public void add(int i, Point3f constr, float weight) {
		assert !hasKey(i);
		
		constraints.put(i, constr);
		weights.put(i, weight);
	}

	public CSRMatrix getMatrix() {
		CSRMatrix m = new CSRMatrix(constraints.size(), n);
		int row = 0;
		for (int i : constraints.keySet()) {
			m.set(row, i, weights.get(i));
			row++;
		}
		return m;
	}

	public List<Float> getVector(int dim, float lambda) {
		assert dim >= 0 && dim <= 2;

		List<Float> b = new ArrayList<>();

		for (int i : constraints.keySet()) {
			b.add(lambda * RigidAlignment.comp(constraints.get(i), dim) *weights.get(i) );
		}

		return b;
	}
	

	public boolean hasKey(int i) {
		return constraints.containsKey(i);
	}
}
