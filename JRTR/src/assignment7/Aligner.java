package assignment7;

import java.io.File;
import java.io.IOException;

import javax.vecmath.Matrix4f;

import meshes.reader.ObjWriter;

public class Aligner {

	/**
	 * 
	 * @param args 	args[0]: path to file containing landmark labels to ignore
	 * 			   	args[1]: path to template .obj 
	 * 				args[2]: path to landmark file of template
	 * 			   	args[i] ... args[i + 1]: paths to i-th face followed by path to landmark file of that face
	 */
	public static void main(String[] args) {
		
		Importer.ignoreLabels(args[0]);
		
		for (int i = 3; i < args.length; ) {
			align(args[1], args[2], args[i++], args[i++]);
		}
		
	}

	private static void align(String templateFile, String templateLandmarks, String faceFile, String faceLandmarks) {
		System.out.println("************ Aligning " + faceFile);
		
		LandmarkedMesh template = Importer.readLandMarkedMesh(templateFile, templateLandmarks);
		LandmarkedMesh face = Importer.readLandMarkedMesh(faceFile, faceLandmarks);
		assert template.equalKeySet(face);
		
		System.out.printf("# of vertices: %d\n", face.getMesh().vertices.size());
		
		// Rigid
		Matrix4f t = RigidAlignment.rigidAlign(template, face);

		face.transform(t);
		save(face, "rigid_aligned_"+getName(faceFile));
		
		final int iterations = 10;
		Warping w = new Warping(template, face);
		for (int i = 0; i < iterations; i++) {
			System.out.println("------------ Running iteration " + i);
			w.iteration();
			if (i < iterations - 1) {
				w.addNewConstraints(i == iterations-2); // in the last iteration's constraint preparation, ignore distance

				System.out.println("Average distance " + w.averageDistance);
			}
		}
		
		// Finally
		String outFolder = "10_iter_laplace_smooth_part1/";
		w.doColorAssignments();
		save(template, outFolder + "template_rigid_to_"+getName(faceFile));
		System.out.println("=== Done");
		
		
	}
	
	private static String getName(String path) {
		return new File(path).getName();
	}

	private static void save(LandmarkedMesh template, String fn) {
		try {
			ObjWriter.write(template.getMesh(), fn);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
