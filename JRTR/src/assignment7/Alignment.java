package assignment7;

import java.io.IOException;

import glWrapper.GLWireframeMesh;

import javax.vecmath.Matrix4f;

import meshes.reader.ObjWriter;
import net.sf.javaml.core.kdtree.KDTree;
import openGL.MyDisplay;

public class Alignment {
	
	private static MyDisplay display;
	
	public static void main(String[] args) {
		
		Importer.ignoreLabel("16","17");
		LandmarkedMesh template = Importer.readLandMarkedMesh("G:/GP Last assignment/headtemplate_large.obj", "G:/student faces/student faces/headtemplate_large_labels");
		LandmarkedMesh face = Importer.readLandMarkedMesh("G:/student faces/student faces/andrian_smile.obj", "G:/student faces/student faces/adrian_smile_labels");
		
		// Rigid
		Matrix4f t = RigidAlignment.rigidAlign(template, face);
		
		// Show
		display = new MyDisplay();
		show(template);
		show(face);
		face.transform(t);
		show(face);
		
		// Non-rigid

		display = new MyDisplay();
		show(face);
		show(template);
		

		
		final int iterations = 10;
		Warping w = new Warping(template, face);

		// Save original template
		w.doColorAssignments();
		save(template, -1);

		// Save in each iteration
		for (int i = 0; i < iterations; i++) {
			System.out.println("------------ Running iteration " + i);
			w.iteration();
			
			show(template);
			
			w.doColorAssignments();
			save(template, i);
			
			if (i < iterations - 1) w.addNewConstraints(i == iterations - 2);
		}
		System.out.println("=== Done");
		
	}

	private static void save(LandmarkedMesh template, int interation) {
		try {
			ObjWriter.write(template.getMesh(), "template" + interation + ".obj");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void show(LandmarkedMesh m) {
		GLWireframeMesh gl = new GLWireframeMesh(m.getMesh());
		
		gl.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		
		display.addToDisplay(gl);
	}

}
