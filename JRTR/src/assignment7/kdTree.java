package assignment7;

import java.util.ArrayList;
import java.util.Stack;

import javax.vecmath.Point3f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

import assignment7.kdTree.Node;
import meshes.WireframeMesh;

public class kdTree {
	WireframeMesh myMesh;
	Node root;
	
	public kdTree(WireframeMesh m) {
		myMesh = m;
		root = new Node(0, 0, myMesh.faces);
	}
	
	float recursiveNN(Node n, Point3f query, Point3f NN, Vector3f NN_normal, float minDist){
		if(n.isLeaf()){
			 return minimalLeafDistance(query, n,minDist, NN, NN_normal);
		}
		else{
			Node nearChild = n.nearChild(query);
			Node farChild = n.farChild(query);
			
			minDist = recursiveNN(nearChild, query, NN, NN_normal, minDist);
			if(n.distanceSqrTo(query ) < minDist){
				minDist = recursiveNN(farChild, query, NN,NN_normal, minDist);
			}
		}
		
		return minDist;
	}
	
	Point3f findNNPos(Point3f query, Vector3f result_normal){
		Point3f pos = new Point3f();
		float minDist = Float.MAX_VALUE;
		
		minDist = recursiveNN(root, query, pos, result_normal, minDist);
		return pos;
	}
	
	public float findNNDist(Point3f query) {
		Point3f pos = new Point3f();
		Vector3f dummyNormal = new Vector3f();
		float minDist = Float.MAX_VALUE;
		
		minDist = recursiveNN(root, query, pos, dummyNormal, minDist);
		return (float) Math.sqrt(minDist);
	}
	
	Point3f findNNPosBruteforce(Point3f v){
		Point3f pos = new Point3f(), candidate;
		float minDist = Float.MAX_VALUE;
		
		Point3f tr[] = new Point3f[3];
		for(int[] t : myMesh.faces){
			tr[0] = myMesh.vertices.get(t[0]);
			tr[1] = myMesh.vertices.get(t[1]);
			tr[2] = myMesh.vertices.get(t[2]);
			candidate = closesPointOnTriangle(tr, v);
			if(candidate.distanceSquared(v)< minDist){
				minDist = candidate.distanceSquared(v);
				pos = candidate;
			}
		}
		return pos;
	}


	private float minimalLeafDistance(Point3f query, Node leaf, float currentDist, Point3f target, Vector3f target_normal) {
		Point3f candidate = new Point3f();
		
		Point3f tr[] = new Point3f[3];
		Vector3f ab = new Vector3f();
		Vector3f ac = new Vector3f();
		for(int[] t: leaf.faces){
			tr[0] = myMesh.vertices.get(t[0]);
			tr[1] = myMesh.vertices.get(t[1]);
			tr[2] = myMesh.vertices.get(t[2]);
			candidate = closesPointOnTriangle(tr, query);
			if( candidate.distanceSquared(query) < currentDist){
				target.set( candidate);
				ab.set(tr[1]);
				ab.sub(tr[0]);
				ac.set(tr[2]);
				ac.sub(tr[0]);
				target_normal.cross(ab, ac);
				target_normal.normalize();
				currentDist =candidate.distanceSquared(query); 
			}
		}
		return currentDist;
	}
	
	


	class Node{
		static final int facesPerLEaf = 5;
		static final int maxLevel = 12;
		float axis_pos;
		int level;
		int axis;
		Node left;
		Node right;
		ArrayList<int[]> faces;
		
		public Node(int level, int axis, ArrayList<int[]> faces) {
			this.axis = axis;
			this.level = level;
			if(faces.size() < facesPerLEaf || level > maxLevel){
				this.faces = faces;
				this.left = null;
				this.right = null;
			}
			else{
				this.faces = null;
				ArrayList<int[]> left = new ArrayList<>();
				ArrayList<int[]> right= new ArrayList<>();
				axis_pos = split(faces, left, right, axis);
				
				this.left = new Node(level+1, (axis+1)%3, left);
				this.right = new Node(level+1, (axis+1)%3, right);
			}
			
		}
		
		public Node farChild(Point3f pos) {
			switch (axis) {
			case 0:
				if(pos.x < axis_pos){
					return right;
				}
				else{
					return left;
				}
			case 1:
				if(pos.y < axis_pos){
					return right;
				}
				else{
					return left;
				}
			default:
				if(pos.z < axis_pos){
					return right;
				}
				else{
					return left;
				}
			}
		}

		public Node nearChild(Tuple3f pos) {
			switch (axis) {
			case 0:
				if(pos.x < axis_pos){
					return left;
				}
				else{
					return right;
				}
			case 1:
				if(pos.y < axis_pos){
					return left;
				}
				else{
					return right;
				}
			default:
				if(pos.z < axis_pos){
					return left;
				}
				else{
					return right;
				}
			}
		}
		public float distanceSqrTo(Point3f pos) {
			assert(axis < 3);
			switch (axis) {
			case 0:
				return (pos.x - axis_pos)*(pos.x - axis_pos);
			case 1:
				return (pos.y - axis_pos)*(pos.y - axis_pos);
				
			case 2:
				return (pos.z - axis_pos)*(pos.z - axis_pos);
				
			}
			return Float.MAX_VALUE;
		}

		private float split(ArrayList<int[]> faces, ArrayList<int[]> left, ArrayList<int[]> right, int axis) {
			float where = mean (myMesh.vertices, faces, axis);
			ArrayList<Point3f> vertices = myMesh.vertices;
			for (int[] f : faces){
				switch (axis) {
				case 0:
					if (vertices.get(f[0]).x <= where || vertices.get(f[1]).x <= where||vertices.get(f[2]).x <= where){
						left.add(f);
					}
					if (vertices.get(f[0]).x >= where || vertices.get(f[1]).x >= where||vertices.get(f[2]).x >= where){
						right.add(f);
					}
				
					break;
				case 1:
					if (vertices.get(f[0]).y <= where || vertices.get(f[1]).y <= where||vertices.get(f[2]).y <= where){
						left.add(f);
					}
					if (vertices.get(f[0]).y >= where || vertices.get(f[1]).y >= where||vertices.get(f[2]).y >= where){
						right.add(f);
					}
					break;
				case 2:
					if (vertices.get(f[0]).z <= where || vertices.get(f[1]).z <= where||vertices.get(f[2]).z <= where){
						left.add(f);
					}
					if (vertices.get(f[0]).z >= where || vertices.get(f[1]).z >= where||vertices.get(f[2]).z >= where){
						right.add(f);
					}
					break;
				}
			}
			
			return where;
		}

		private float mean(ArrayList<Point3f> vertices, ArrayList<int[]> faces, int axis) {
			float mean = 0;
			for (int[] f : faces){
				switch (axis) {
				case 0:
					mean += vertices.get(f[0]).x;
					mean += vertices.get(f[1]).x;
					mean += vertices.get(f[2]).x;
					break;
				case 1:
					mean += vertices.get(f[0]).y;
					mean += vertices.get(f[1]).y;
					mean += vertices.get(f[2]).y;
					break;
				case 2:
					mean += vertices.get(f[0]).z;
					mean += vertices.get(f[1]).z;
					mean += vertices.get(f[2]).z;
					break;
				}
			}
			mean/= faces.size() * 3;
			return mean;
		}

		boolean isLeaf(){
			return faces != null;
		}
	}
	
	
	
	Point3f closesPointOnTriangle(Point3f[] tr, Tuple3f v )
	{
	    Vector3f edge0 = new Vector3f(tr[1]);
	    edge0.sub(tr[0]);
	    Vector3f edge1 = new Vector3f(tr[2]);
	    edge1.sub(tr[0]);
	    Vector3f v0 = new Vector3f(tr[0]);
	    v0.sub(v);

	    float a = edge0.dot( edge0 );
	    float b = edge0.dot( edge1 );
	    float c = edge1.dot( edge1 );
	    float d = edge0.dot( v0 );
	    float e = edge1.dot( v0 );

	    float det = a*c - b*b;
	    float s = b*e - c*d;
	    float t = b*d - a*e;

	    if ( s + t < det )
	    {
	        if ( s < 0.f )
	        {
	            if ( t < 0.f )
	            {
	                if ( d < 0.f )
	                {
	                    s = clamp( -d/a, 0.f, 1.f );
	                    t = 0.f;
	                }
	                else
	                {
	                    s = 0.f;
	                    t = clamp( -e/c, 0.f, 1.f );
	                }
	            }
	            else
	            {
	                s = 0.f;
	                t = clamp( -e/c, 0.f, 1.f );
	            }
	        }
	        else if ( t < 0.f )
	        {
	            s = clamp( -d/a, 0.f, 1.f );
	            t = 0.f;
	        }
	        else
	        {
	            float invDet = 1.f / det;
	            s *= invDet;
	            t *= invDet;
	        }
	    }
	    else
	    {
	        if ( s < 0.f )
	        {
	            float tmp0 = b+d;
	            float tmp1 = c+e;
	            if ( tmp1 > tmp0 )
	            {
	                float numer = tmp1 - tmp0;
	                float denom = a-2*b+c;
	                s = clamp( numer/denom, 0.f, 1.f );
	                t = 1-s;
	            }
	            else
	            {
	                t = clamp( -e/c, 0.f, 1.f );
	                s = 0.f;
	            }
	        }
	        else if ( t < 0.f )
	        {
	            if ( a+d > b+e )
	            {
	                float numer = c+e-b-d;
	                float denom = a-2*b+c;
	                s = clamp( numer/denom, 0.f, 1.f );
	                t = 1-s;
	            }
	            else
	            {
	                s = clamp( -e/c, 0.f, 1.f );
	                t = 0.f;
	            }
	        }
	        else
	        {
	            float numer = c+e-b-d;
	            float denom = a-2*b+c;
	            s = clamp( numer/denom, 0.f, 1.f );
	            t = 1.f - s;
	        }
	    }

	    if(s*0 != 0){
	    	s = 0;
	    }
	    if(t * 0 != 0){
	    	t = 0;
	    }
	    Point3f pos = new Point3f(tr[0]);
	    pos.x += s * edge0.x + t * edge1.x;
	    pos.y += s * edge0.y + t * edge1.y;
	    pos.z += s * edge0.z + t * edge1.z;
	    return pos;
	}



	private float clamp(float f, float g, float h) {
		return (
					f < g ? g:
					f > h ? h:
					f);
	}


	
}
