package assignment7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.vecmath.Matrix4f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;

public class LandmarkedMesh {

	private Map<String, Integer> landmarks;
	private WireframeMesh mesh;
	private HalfEdgeStructure hs;

	public LandmarkedMesh(Map<String, Integer> landmarks, WireframeMesh mesh) {
		super();
		this.landmarks = landmarks;
		this.mesh = mesh;

		try {

			hs = new HalfEdgeStructure();
			hs.init(mesh);
		} catch (MeshNotOrientedException | DanglingTriangleException e) {
			hs = null;
		}

		assert null != hs || !mesh.normals.isEmpty();
	}

	public Vector3f getNormal(int index) {
		if (null != hs) {
			Vector3f normal = hs.getVertices().get(index).laplaceBeltrami();
			normal.normalize();
			return normal;
		}
		return mesh.normals.get(index);
	}

	public Point3f getPosition(String landmarkLabel) {
		return mesh.vertices.get(landmarks.get(landmarkLabel));
	}

	public int getIndex(String landmarkLabel) {
		return landmarks.get(landmarkLabel);
	}

	public List<Point3f> getLandMarkPositions() {
		ArrayList<String> keys = new ArrayList<>();
		keys.addAll(landmarks.keySet());

		Collections.sort(keys);

		ArrayList<Point3f> positions = new ArrayList<Point3f>();
		for (String key : keys) {
			positions.add(getPosition(key));
		}

		return positions;
	}

	public Iterable<String> getLabels() {
		return landmarks.keySet();
	}

	public boolean equalKeySet(LandmarkedMesh other) {
		return landmarks.keySet().equals(other.landmarks.keySet());
	}

	public void transform(Matrix4f t) {
		for (Point3f p : mesh.vertices) {
			t.transform(p);
		}
	}

	public WireframeMesh getMesh() {
		return this.mesh;
	}

	public HalfEdgeStructure getHs() {
		return this.hs;
	}

}
