package assignment7;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import com.jogamp.common.util.Function;

import sparse.CSRMatrix;
import sparse.solver.LSQRSolver;
import sparse.solver.MATLABSolver;
import sparse.solver.Solver;
import assignment4.LMatrices;
import net.sf.javaml.core.kdtree.KDTree;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;

public class Warping {

	public static float LAMBDA = 10, 
			// Add constraints adds all vertices closer than THR_DIST_PERCENTAGE * average distance
			THR_DIST_PERCENTAGE = 0.8f,
			THR_PARALLEL_ANGLE_TOL = 0.6f;
	
	private KDTree faceKDtree, templateKDtree;
	private kdTree own_face_tree;
	private LandmarkedMesh template, face;
	private WarpingConstraints constr;
	
	private static KDTree forMesh(LandmarkedMesh m) {
		KDTree KDtree = new KDTree(3);
		for (int i = 0; i < m.getMesh().vertices.size(); i++) {
			Point3f p = m.getMesh().vertices.get(i);
			KDtree.insert(toArray(p), i);
		}
		return KDtree;
	}
	
	public Warping(LandmarkedMesh template, LandmarkedMesh face) {
		this.template = template; this.face = face;
		// Contruct acceleration
		faceKDtree = forMesh(face);
		templateKDtree = forMesh(template);

		own_face_tree = new kdTree(face.getMesh());
		// Build initial constraints
		assert template.equalKeySet(face);
		constr = new WarpingConstraints(template.getMesh().vertices.size());
		// Constrain landmarks
		for (String label : face.getLabels()) {
			int index = template.getIndex(label);
			constr.add(index, face.getPosition(label), 1.f);
		}
		// constrain boundary?
		for (Vertex v : template.getHs().getVertices()) {
			if (!v.isOnBorder()) continue;
			
			// Constrain it 
			int i = v.index;
			Point3f p = v.getPos();
			// Make sure indices stayed the same
			assert p.epsilonEquals(
					template.getMesh().vertices.get(i)
					, 0.000001f);
			
			// add constr
			constr.add(i, p, 10);
		}
	}

	private static double[] toArray(Point3f p) {
		return new double[] { p.x, p.y, p.z };
	}

	private float dist(Point3f a, Point3f b) {
		Vector3f c = new Vector3f(a); c.sub(b);
		return c.length();
	}
	
	public static boolean USE_FACE_TO_TEMPLATE_COLOR = false;
	public void doColorAssignments() {
		// For each point, take the color of the constraining point
		// TODO how can we have a color for non-constrained points
		

		final int nTemplate = template.getMesh().vertices.size();
		// Fill template colors array with zero
		template.getMesh().colors.clear();
		for (Point3f p : template.getMesh().vertices) {
			template.getMesh().colors.add(new Point3f());
		}
		assert template.getMesh().colors.size() == nTemplate;
		

		final int [] colorCount = new int[nTemplate];
		if (USE_FACE_TO_TEMPLATE_COLOR) {
			// Sum up color of face vertices in closest template vertex
			// Keep a count.
			for (int i = 0; i < face.getMesh().vertices.size(); i++) {
				Point3f p = face.getMesh().vertices.get(i);
				Point3f color = face.getMesh().colors.get(i);
				
				int j = getClosestTemplateIndex(p);
				
				// Add up colors
				template.getMesh().colors.get(j).add(color);
				colorCount[j]++;
			}
		}
		
		// For those vertices in template that have no color assigned yet, assign the color 
		// of the closest face vertex (we could also do just this)
		for (int i = 0; i < nTemplate; i++) {
			if (colorCount[i] > 0) continue;

			Point3f p = template.getMesh().vertices.get(i);
			int j = getClosestFaceIndex(p);
			Point3f color = face.getMesh().colors.get(j);
			
			template.getMesh().colors.set(i, color);
			
			colorCount[i] = 1;
		}
		
		// Compute average
		for (int i = 0; i < nTemplate; i++) {
			Point3f color = template.getMesh().colors.get(i);
			assert colorCount[i] > 0;
			color.scale(1.f / colorCount[i]);
		}
	}

	public void addNewConstraints(boolean ignoreDistance) {
		addNewConstraints(constr, template, face, ignoreDistance);
	}


	public float averageDistance;
	private void addNewConstraints(WarpingConstraints constr, LandmarkedMesh template, LandmarkedMesh face, boolean ignoreDistance) {
		kdTree boundary_tree = new kdTree(boundary(template));
		
		// Computer average distance to closest point
		averageDistance = 0;
		for (Point3f p : template.getMesh().vertices) {
			averageDistance += dist(p, getClosestFacePosition(p));
		}
		averageDistance /= template.getMesh().vertices.size();
		
		// Add constraints: All vertices closer than THR_DIST_PERCENTAGE * average distance
		// with a similar normal
		for (int i = 0; i < template.getMesh().vertices.size(); i++) {
			//commented out because I want weights to adapt.
			//if (constr.hasKey(i)) {
			//	continue;
			//}
			if(template.getHs().getVertices().get(i).isOnBorder()){
				continue;
			}
			Point3f templatePoint = template.getMesh().vertices.get(i);
			Vector3f templateNormal = template.getNormal(i);
			Vector3f faceNormal = new Vector3f();
			Point3f closestOnFace = own_face_tree.findNNPos(templatePoint, faceNormal);
			
			Vector3f diff = new Vector3f(closestOnFace);
			diff.sub(templatePoint);
			
			if (!ignoreDistance && diff.length() > averageDistance * THR_DIST_PERCENTAGE) 
				continue;
			
			if(templateNormal.dot(faceNormal) < THR_PARALLEL_ANGLE_TOL) 
				continue;
			
			diff.normalize();
			if(Math.abs(diff.dot(templateNormal)) < 0.5f) continue;
			
			float sigma = 2.5f;
			float distToBoundary = boundary_tree.findNNDist(templatePoint);
			//sigmoid of distance. at the boundary this function will be close to zero, at sigma it will be 0.5
			distToBoundary = (float) (1/(1+ Math.exp(- (distToBoundary - sigma)*(6/sigma))));
			constr.add(i,closestOnFace, (float) Math.pow(templateNormal.dot(faceNormal),10) * distToBoundary);
		}
		System.out.println("constraints: " + constr.count());
		
		return;
	}

	private WireframeMesh boundary(LandmarkedMesh m) {
		WireframeMesh boundary = new WireframeMesh();
		HalfEdgeStructure hs = m.getHs();
		
		for(Vertex v: hs.getVertices()){
			if(v.isOnBorder()){
				int[] f = {boundary.vertices.size(),boundary.vertices.size(),boundary.vertices.size()};
				boundary.vertices.add(v.getPos());
				boundary.faces.add(f);
			}
		}
		
		return boundary;
	}

	private Point3f getClosestFacePosition(Point3f point3f) {
		int closest = getClosestFaceIndex(point3f);
		return face.getMesh().vertices.get(closest);
	}

	// Run an iteration of nonrigid deformation with constraints
	public void iteration() {
		iteration(template.getMesh(), face.getMesh(), constr);
	}
	
	 private static ExecutorService es = Executors.newFixedThreadPool(3);
	 private static void forEachDimension(final Function<Void, Integer> f) {
	  final CountDownLatch latch = new CountDownLatch(3);
	  for (int d = 0; d < 3; d++) {
	   final int dd = d;
	   es.execute(new Runnable() {
		
			@Override
			public void run() {
				f.eval(dd); 
				latch.countDown();
			}
		  });
	  }
	  
	  try {
	     latch.await();
	  } catch (InterruptedException E) {}
	 }
	 
	private void iteration(WireframeMesh template, WireframeMesh face, final WarpingConstraints constr) {
		// Get L
		HalfEdgeStructure hs = new HalfEdgeStructure();
		try {
			hs.init(template);
		} catch (MeshNotOrientedException | DanglingTriangleException e) {
			e.printStackTrace();
		}
		//final CSRMatrix laplacian = LMatrices.mixedCotanLaplacian(hs);
		final CSRMatrix laplacian = LMatrices.unnormalizedCotanLaplacian(hs);

		// A is L followed by constraint definition
		final CSRMatrix A = new CSRMatrix(0, laplacian.nCols);
		A.append(laplacian, 1);
		A.append(constr.getMatrix(), LAMBDA);
		//assert A.nRows() == laplacian.nRows() + constr.

		// Solving Ax' = b
		final ArrayList<Float>[] points = p(template);
		final ArrayList<Float>[] newPoints = new ArrayList[3];

		
		for (int dim = 0; dim <= 2; dim++) {
				// b is Lx followed by lambda * c
				ArrayList<Float> b = new ArrayList<Float>();
				laplacian.mult(points[dim], b);

				b.addAll(constr.getVector(dim, LAMBDA));

				// Solve
				newPoints[dim] = new ArrayList<Float>();
				

				final Solver solver = new MATLABSolver();//new MATLABSolver();
				solver.solve(A, b, newPoints[dim]);
		}
		
		// Modify in-place
		apply_p(newPoints, template);

	}

	private int getClosestFaceIndex(Point3f p) {
		return (int) faceKDtree.nearest(toArray(p));
	}
	
	private int getClosestTemplateIndex(Point3f p) {
		return (int) templateKDtree.nearest(toArray(p));
	}

	public static ArrayList<Float>[] p(WireframeMesh m) {
		ArrayList<Float>[] p = new ArrayList[3];
		for (int i = 0; i < 3; i++)
			p[i] = new ArrayList<Float>();

		for (Point3f v : m.vertices) {
			p[0].add(v.x);
			p[1].add(v.y);
			p[2].add(v.z);
		}
		return p;
	}

	public static void apply_p(ArrayList<Float>[] p, WireframeMesh m) {
		final int n = m.vertices.size();
		assert p[0].size() == n;
		assert p[1].size() == n;
		assert p[2].size() == n;
		assert p.length == 3;

		for (int i = 0; i < n; i++) {
			m.vertices.set(i, new Point3f(p[0].get(i), p[1].get(i), p[2].get(i)));
		}
	}

}
