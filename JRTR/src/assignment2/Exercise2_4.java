package assignment2;

import glWrapper.GLHashtree;
import glWrapper.GLHashtree_Cells;
import glWrapper.GLHashtree_Vertices;

import java.io.IOException;

import meshes.PointCloud;
import meshes.reader.PlyReader;
import openGL.MyDisplay;

public class Exercise2_4 {

	public static void main(String[] args) throws IOException {

		PointCloud pc = PlyReader.readPointCloud("./objs/octreeTest2.ply", true);
		
		HashOctree tree = new HashOctree(pc, 4, 1, 1f);

		MyDisplay display = new MyDisplay();
		GLHashtree_Vertices glHashTree = new GLHashtree_Vertices(tree);
		GLHashtree glOTcopy = new GLHashtree(tree);
		
		glHashTree.addVertexToVertexAdjacency();

		glHashTree.configurePreferredShader("shaders/octree_v2v.vert", "shaders/octree_v2v.frag", "shaders/octree_v2v.geom");
		glOTcopy.configurePreferredShader("shaders/octree.vert", "shaders/octree.frag", "shaders/octree.geom");
		
		display.addToDisplay(glHashTree);
		display.addToDisplay(glOTcopy);

	}
}