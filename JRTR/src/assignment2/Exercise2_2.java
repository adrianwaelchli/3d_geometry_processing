package assignment2;

import glWrapper.GLHashtree;
import glWrapper.GLHashtree_Cells;

import java.io.IOException;

import meshes.PointCloud;
import meshes.reader.PlyReader;
import openGL.MyDisplay;

public class Exercise2_2 {

	public static void main(String[] args) throws IOException {

		PointCloud pc = PlyReader.readPointCloud("./objs/octreeTest2.ply", true);

		HashOctree tree = new HashOctree(pc, 4, 1, 1f);

		MyDisplay display = new MyDisplay();
		GLHashtree_Cells glOT = new GLHashtree_Cells(tree);
		GLHashtree glOTcopy = new GLHashtree(tree);
		
		glOT.addCellParentPositions();

		glOT.configurePreferredShader("shaders/octree_parent_cell.vert", "shaders/octree_parent_cell.frag", "shaders/octree_parent_cell.geom");
		glOTcopy.configurePreferredShader("shaders/octree.vert", "shaders/octree.frag", "shaders/octree.geom");
		
		display.addToDisplay(glOT);
		display.addToDisplay(glOTcopy);

	}
}
