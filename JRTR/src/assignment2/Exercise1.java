package assignment2;

import java.io.IOException;
import glWrapper.GLHashtree;
import glWrapper.GLHashtree_Vertices;
import glWrapper.GLPointCloud;
import meshes.PointCloud;
import meshes.reader.ObjReader;
import meshes.reader.PlyReader;
import openGL.MyDisplay;
import static org.junit.Assert.*;

public class Exercise1 {

	public static void main(String[] args) throws IOException {

		mortonCodesDemo();

		hashTreeDemo(ObjReader.readAsPointCloud("./objs/dragon.obj", true));
		hashTreeDemo(PlyReader.readPointCloud("./objs/octreeTest2.ply", true));

	}

	public static void mortonCodesDemo() {

		// Example of a level 4 morton code
		long hash = 0b1000101000100;

		// The hashes of its parent and neighbors
		long parent = 0b1000101000;
		long nbr_plus_x = 0b1000101100000;
		long nbr_plus_y = 0b1000101000110;
		long nbr_plus_z = 0b1000101000101;

		long nbr_minus_x = 0b1000101000000;
		long nbr_minus_y = -1; // invalid: the vertex lies on the boundary and
								// an underflow should occur
		long nbr_minus_z = 0b1000100001101;

		// Example of a a vertex morton code in a multigrid of depth 4. It lies
		// on the level 3 and 4 grids.
		long vertexHash = 0b1000110100000;

		// Further test at least one case where -z underflow should occur
		// and a case where overflow occurs.

		// Tests for parentCode(...)
		assertEquals(MortonCodes.parentCode(hash), parent);
		assertEquals(MortonCodes.parentCode(0b1), -1L);

		// Tests for nbrCode(...)
		assertEquals(MortonCodes.nbrCode(0b1000, 1, 0b100), 0b1100);
		assertEquals(MortonCodes.nbrCode(0b1000, 1, 0b010), 0b1010);
		assertEquals(MortonCodes.nbrCode(0b1000, 1, 0b110), 0b1110);

		// Tests for overflow
		assertEquals(MortonCodes.nbrCode(0b1110, 1, 0b100), -1);
		assertEquals(MortonCodes.nbrCode(0b1110, 1, 0b010), -1);
		assertEquals(MortonCodes.nbrCode(0b1001, 1, 0b001), -1);
		
		assertEquals(MortonCodes.nbrCode(0b1, 0, 0b100), -1);
		assertEquals(MortonCodes.nbrCode(0b1, 0, 0b010), -1);
		assertEquals(MortonCodes.nbrCode(0b1, 0, 0b001), -1);

		// Tests for underflow
		assertEquals(MortonCodes.nbrCodeMinus(0b1000, 1, 0b100), -1);
		assertEquals(MortonCodes.nbrCodeMinus(0b1000, 1, 0b010), -1);
		assertEquals(MortonCodes.nbrCodeMinus(0b1000, 1, 0b001), -1);

		// Tests for isCellOnLevelXGrid(...)
		assertTrue(MortonCodes.isCellOnLevelXGrid(hash, 4));
		assertTrue(MortonCodes.isCellOnLevelXGrid(0b1, 0));

		// Tests for isVertexOnLevelXGrid(...)
		assertTrue(MortonCodes.isVertexOnLevelXGrid(vertexHash, 3, 4));
		assertTrue(MortonCodes.isVertexOnLevelXGrid(vertexHash, 4, 4));
		assertFalse(MortonCodes.isVertexOnLevelXGrid(vertexHash, 2, 4));
		assertFalse(MortonCodes.isVertexOnLevelXGrid(vertexHash, 1, 4));
		assertFalse(MortonCodes.isVertexOnLevelXGrid(vertexHash, 5, 4));

	}

	public static void hashTreeDemo(PointCloud pc) {

		HashOctree tree = new HashOctree(pc, 4, 1, 1f);

		MyDisplay display = new MyDisplay();
		GLPointCloud glPC = new GLPointCloud(pc);
		GLHashtree glOT = new GLHashtree(tree);

		glOT.configurePreferredShader("shaders/octree.vert", "shaders/octree.frag", "shaders/octree.geom");

		GLHashtree_Vertices glOTv = new GLHashtree_Vertices(tree);

		display.addToDisplay(glOT);
		display.addToDisplay(glOTv);

		display.addToDisplay(glPC);

	}

}
