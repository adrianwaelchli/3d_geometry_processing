package assignment2;

import glWrapper.GLHashtree;
import glWrapper.GLHashtree_Cells;

import java.io.IOException;

import meshes.PointCloud;
import meshes.reader.PlyReader;
import openGL.MyDisplay;

public class Exercise2_3 {

	public static void main(String[] args) throws IOException {

		PointCloud pc = PlyReader.readPointCloud("./objs/octreeTest2.ply", true);
		
		HashOctree tree = new HashOctree(pc, 4, 1, 1f);

		MyDisplay display = new MyDisplay();
		GLHashtree_Cells glHashTree = new GLHashtree_Cells(tree);
		GLHashtree glOTcopy = new GLHashtree(tree);
		
		glHashTree.addCellToCellAdjacency();

		glHashTree.configurePreferredShader("shaders/octree_c2c.vert", "shaders/octree_c2c.frag", "shaders/octree_c2c.geom");
		glOTcopy.configurePreferredShader("shaders/octree.vert", "shaders/octree.frag", "shaders/octree.geom");
		
		display.addToDisplay(glHashTree);
		display.addToDisplay(glOTcopy);

	}
}
