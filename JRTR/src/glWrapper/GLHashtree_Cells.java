package glWrapper;

import java.util.ArrayList;

import javax.media.opengl.GL;

import openGL.gl.GLDisplayable;
import openGL.gl.GLRenderer;
import openGL.objects.Transformation;

import assignment2.HashOctree;
import assignment2.HashOctreeCell;

/**
 * Simple GLWrapper for the {@link HashOctree}. The octree is sent to OpenGL as
 * a set of center positions of the cells.
 * 
 * @author Adrian Waelchli
 *
 */
public class GLHashtree_Cells extends GLDisplayable {

	private final HashOctree tree;

	public GLHashtree_Cells(HashOctree tree) {

		super(tree.numberOfCells());
		this.tree = tree;

		float[] glVertices = new float[tree.numberOfCells() * 3];
		float[] sides = new float[tree.numberOfCells()];

		int idx = 0, idx2 = 0;
		for (HashOctreeCell cell : tree.getCells()) {
			glVertices[idx++] = cell.center.x;
			glVertices[idx++] = cell.center.y;
			glVertices[idx++] = cell.center.z;
			sides[idx2++] = cell.side;
		}

		int[] glIndices = new int[tree.numberOfCells()];
		for (int i = 0; i < glIndices.length; i++) {
			glIndices[i] = i;
		}

		this.addElement(glVertices, Semantic.POSITION, 3);
		this.addElement(sides, Semantic.USERSPECIFIED, 1, "side");
		this.addIndices(glIndices);
	}

	/**
	 * values are given by OctreeVertex
	 * 
	 * @param values
	 */
	public void addFunctionValues(ArrayList<Float> values) {
		float[] vals = new float[tree.numberOfLeafs()];

		for (HashOctreeCell n : tree.getLeafs()) {
			for (int i = 0; i <= 0b111; i++) {
				vals[n.leafIndex] += values.get(tree.getNbr_c2v(n, i).index);// */Math.signum(values.get(myTree.getVertex(n,
																				// i).index));
			}
			vals[n.leafIndex] /= 8;
			// vals[n.leafIndex] = Math.abs(vals[n.leafIndex]) < 5.99 ? -1: 1;
		}

		this.addElement(vals, Semantic.USERSPECIFIED, 1, "func");
	}

	public void addCellParentPositions() {

		float[] glParentPositions = new float[3 * tree.numberOfCells()];

		int idx = 0;
		for (HashOctreeCell cell : tree.getCells()) {

			HashOctreeCell parent = tree.getParent(cell);
			/*
			 * If the cell has no parent, add the position of the cell.
			 */
			parent = parent == null ? cell : parent;

			glParentPositions[idx++] = parent.center.x;
			glParentPositions[idx++] = parent.center.y;
			glParentPositions[idx++] = parent.center.z;
		}

		this.addElement(glParentPositions, Semantic.USERSPECIFIED, 3, "parentPosition");
	}

	public void addCellToCellAdjacency() {
		addCellToCellAdjacency(0b100, 1);
		addCellToCellAdjacency(0b010, 1);
		addCellToCellAdjacency(0b001, 1);

		addCellToCellAdjacency(0b100, -1);
		addCellToCellAdjacency(0b010, -1);
		addCellToCellAdjacency(0b001, -1);
	}

	private void addCellToCellAdjacency(int Obxyz, float signum) {

		float[] glCellToCellAdj = new float[3 * tree.numberOfCells()];

		int c = 0;
		for (HashOctreeCell cell : tree.getCells()) {

			HashOctreeCell nbr = signum >= 0 ? tree.getNbr_c2c(cell, Obxyz) : tree.getNbr_c2cMinus(cell, Obxyz);

			if (nbr == null)
				nbr = cell;

			glCellToCellAdj[c++] = nbr.center.x;
			glCellToCellAdj[c++] = nbr.center.y;
			glCellToCellAdj[c++] = nbr.center.z;
		}

		String varName = "position_c2c_";
		if (signum < 0)
			varName += "minus_";

		varName += (Obxyz >> 2) & 1;
		varName += (Obxyz >> 1) & 1;
		varName += (Obxyz >> 0) & 1;
		this.addElement(glCellToCellAdj, Semantic.USERSPECIFIED, 3, varName);
	}

	public int glRenderFlag() {
		return GL.GL_POINTS;
	}

	@Override
	public void loadAdditionalUniforms(GLRenderer glRenderContext, Transformation mvMat) {

	}
}
