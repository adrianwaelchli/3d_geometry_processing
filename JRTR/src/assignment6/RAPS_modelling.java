package assignment6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import assignment4.LMatrices;
import meshes.HalfEdge;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import sparse.CSRMatrix;

/**
 * As rigid as possible deformations.
 * 
 * @author Alf
 *
 */
public class RAPS_modelling {

	// ArrayList containing all optimized rotations,
	// keyed by vertex.index
	ArrayList<Matrix3f> rotations;

	// keep a copy of the original halfEdge structure.
	// The original halfedge structure is needed to compute the correct
	// rotation matrices.
	private HalfEdgeStructure hs_original, hs_deformed;

	// The unnormalized cotan weight matrix, with zero rows for
	// boundary vertices.
	// It can be computed once at setup time and then be reused
	// to compute the matrix needed for position optimization
	CSRMatrix L_cotan;
	// The deformed mesh
	CSRMatrix L_deform;

	// allocate righthand sides and x only once.
	ArrayList<Float>[] b;
	ArrayList<Float> x;

	// sets of vertex indices that are constrained.
	private HashSet<Integer> keepFixed;
	private HashSet<Integer> deform;

	private float constraintWeight = 10000;

	private Cholesky solver;

	/**
	 * The mesh to be deformed
	 * 
	 * @param hs
	 */
	public RAPS_modelling(HalfEdgeStructure hs) {

		// Deep copy the original mesh
		this.hs_original = new HalfEdgeStructure(hs);
		this.hs_deformed = hs;

		this.keepFixed = new HashSet<>();
		this.deform = new HashSet<>();

		rotations = new ArrayList<>();
		for (Vertex v : hs_deformed.getVertices()) {
			rotations.add(new Matrix3f());
		}

		resetRotations();

		init_b_x(hs);
		L_cotan = LMatrices.unnormalizedCotanLaplacian(hs);
	}

	public void setConstraintsWeight(float weight) {
		this.constraintWeight = weight;
	}

	/**
	 * Set which vertices should be kept fixed.
	 * 
	 * @param verts_idx
	 */
	public void keep(Collection<Integer> verts_idx) {
		this.keepFixed.clear();
		this.keepFixed.addAll(verts_idx);

	}

	/**
	 * constrain these vertices to the new target position
	 */
	public void target(Collection<Integer> vert_idx) {
		this.deform.clear();
		this.deform.addAll(vert_idx);
	}

	/**
	 * update the linear system used to find optimal positions for the currently
	 * constrained vertices. Good place to do the cholesky decompositoin
	 */
	public void updateL() {

		L_deform = new CSRMatrix(L_cotan.nRows(), L_cotan.nCols());

		/*
		 * Compute left-hand side of the linear system
		 */
		CSRMatrix L_transposed = L_cotan.transposed();
		CSRMatrix L_transposed_L = new CSRMatrix(L_transposed.nRows(), L_transposed.nCols());
		// L^T * L
		L_transposed.mult(L_cotan, L_transposed_L);

		// w^2 * I_constr
		CSRMatrix w_squared_I_constr = computeI_constr(constraintWeight * constraintWeight);

		L_deform.add(L_transposed_L, w_squared_I_constr);

		solver = new Cholesky(L_deform);
	}

	/**
	 * The RAPS modelling algorithm.
	 * 
	 * @param t
	 * @param nRefinements
	 */
	public void deform(Matrix4f t, int nRefinements) {
		this.transformTarget(t);

		for (int i = 0; i < nRefinements; i++) {
			optimalRotations();
//			updateL();
			optimalPositions();
		}
	}

	/**
	 * Method to transform the target positions and do nothing else.
	 * 
	 * @param t
	 */
	public void transformTarget(Matrix4f t) {
		for (Vertex v : hs_deformed.getVertices()) {
			if (deform.contains(v.index)) {
				t.transform(v.getPos());
			}
		}
	}

	/**
	 * ArrayList keyed with the vertex indices.
	 * 
	 * @return
	 */
	public ArrayList<Matrix3f> getRotations() {
		return rotations;
	}

	/**
	 * Getter for undeformed version of the mesh
	 * 
	 * @return
	 */
	public HalfEdgeStructure getOriginalCopy() {
		return hs_original;
	}

	/**
	 * initialize b and x
	 * 
	 * @param hs
	 */
	private void init_b_x(HalfEdgeStructure hs) {
		b = new ArrayList[3];
		for (int i = 0; i < 3; i++) {
			b[i] = new ArrayList<>(hs.getVertices().size());
			for (int j = 0; j < hs.getVertices().size(); j++) {
				b[i].add(0.f);
			}
		}
		x = new ArrayList<>(hs.getVertices().size());
		for (int j = 0; j < hs.getVertices().size(); j++) {
			x.add(0.f);
		}
	}

	/**
	 * Compute optimal positions for the current rotations.
	 */
	public void optimalPositions() {
		int n = hs_original.getVertices().size();

		compute_b();

		/*
		 * Compute L^T * b + w^2 * I_constr * p_constr
		 */
		CSRMatrix L_transpose = L_cotan.transposed();

		ArrayList<Float>[] righthandSide = new ArrayList[3];
		righthandSide[0] = initZero(n);
		righthandSide[1] = initZero(n);
		righthandSide[2] = initZero(n);

		L_transpose.mult(b, righthandSide);

		HashSet<Integer> constr = new HashSet<Integer>();
		constr.addAll(keepFixed);
		constr.addAll(deform);

		// L^T * b + w^2 * I_constr * p_constr
		for (int constr_index : constr) {
			Point3f p_constr = new Point3f(hs_deformed.getVertices().get(constr_index).getPos());
			// w^2 * p_constr
			p_constr.scale(constraintWeight * constraintWeight);

			// L^T * b + w^2 * p_constr
			righthandSide[0].set(constr_index, righthandSide[0].get(constr_index) + p_constr.x);
			righthandSide[1].set(constr_index, righthandSide[1].get(constr_index) + p_constr.y);
			righthandSide[2].set(constr_index, righthandSide[2].get(constr_index) + p_constr.z);
		}

		ArrayList<Float> pointsX = initZero(n);
		ArrayList<Float> pointsY = initZero(n);
		ArrayList<Float> pointsZ = initZero(n);

		solver.solve(righthandSide[0], pointsX);
		solver.solve(righthandSide[1], pointsY);
		solver.solve(righthandSide[2], pointsZ);

		/*
		 * Update positions
		 */
		for (Vertex vertex : hs_deformed.getVertices()) {
			vertex.getPos().x = pointsX.get(vertex.index);
			vertex.getPos().y = pointsY.get(vertex.index);
			vertex.getPos().z = pointsZ.get(vertex.index);
		}

	}

	public void compute_b() {
		reset_b();

		for (Vertex vertex : hs_original.getVertices()) {

			if (vertex.isOnBorder()) {
				// Zero for boundary vertices
				continue;
			}

			int i = vertex.index;
			Iterator<HalfEdge> iterator = vertex.iteratorVE();

			// b_i
			Vector3f sum = new Vector3f();

			while (iterator.hasNext()) {
				HalfEdge edge = iterator.next();
				Vertex neighbor = edge.end();
				int j = neighbor.index;
				float w_ij = L_cotan.get(i, j);
				Matrix3f rot = new Matrix3f();
				// R_i + R_j
				rot.add(getRotations().get(i), getRotations().get(j));
				// p_i - p_j
				Vector3f vec = edge.vector();
				// vec.negate();
				// (R_i + R_j)(p_i - p_j)
				rot.transform(vec);
				// 0.5 * w_ij * (R_i + R_j)(p_i - p_j)
				vec.scale(0.5F * w_ij);

				sum.add(vec);
			}

			b[0].set(i, sum.x);
			b[1].set(i, sum.y);
			b[2].set(i, sum.z);
		}
	}

	/**
	 * helper method
	 */
	private void reset_b() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < b[i].size(); j++) {
				b[i].set(j, 0.f);
			}
		}
	}

	/**
	 * helper method
	 */
	public void resetRotations() {
		for (Matrix3f r : rotations) {
			r.setIdentity();
		}
	}

	/**
	 * Compute the optimal rotations for 1-neighborhoods, given the original and
	 * deformed positions.
	 */
	public void optimalRotations() {
		resetRotations();

		/*
		 * Argument controls number of iterations for ed/svd decompositions. 3 =
		 * very low precision but high speed. 3 seems to be good enough.
		 */
		Linalg3x3 l = new Linalg3x3(10);

		for (Vertex vertex : hs_original.getVertices()) {

			int i = vertex.index;

			if (vertex.isOnBorder() || keepFixed.contains(i)) {
				continue;
			}

			Matrix3f S_i = new Matrix3f();

			Iterator<HalfEdge> edgeIt = vertex.iteratorVE();
			while (edgeIt.hasNext()) {
				HalfEdge edge = edgeIt.next();
				Vertex neighbor = edge.end();

				int j = neighbor.index;

				// e_ij = p_i - p_j and e_ij' = p_i' - p_j'
				Vector3f e_ij = edge.vector();
				e_ij.negate();
				Point3f p_i_prime = hs_deformed.getVertices().get(i).getPos();
				Point3f p_j_prime = hs_deformed.getVertices().get(j).getPos();
				Vector3f e_ij_prime = new Vector3f(p_i_prime);
				e_ij_prime.sub(p_j_prime);

				/*
				 * Slightly better results are achieved when the absolute of
				 * cotangent weights w_ij are used instead of plain cotangent
				 * weights.
				 */
				float w_ij = Math.abs(L_cotan.get(i, j));

				// e_ij * e_ij'^T
				Matrix3f outerProduct = new Matrix3f();
				compute_ppT(e_ij, e_ij_prime, outerProduct);

				outerProduct.mul(w_ij);
				S_i.add(outerProduct);
			}

			/*
			 * Compute R_i from S_i via svd
			 */
			Matrix3f U_i = new Matrix3f();
			Matrix3f V_i = new Matrix3f();
			Matrix3f Sigma_i = new Matrix3f();

			l.svd(S_i, U_i, Sigma_i, V_i);

			if (U_i.determinant() < 0) {
				scaleLastColumn(U_i, -1);
			}

			// R_i = V_i * U_i^T
			Matrix3f R_i = new Matrix3f(V_i);
			U_i.transpose();
			R_i.mul(U_i);

			rotations.set(i, R_i);
		}
	}

	private void compute_ppT(Vector3f p, Vector3f p2, Matrix3f pp2T) {
		assert(p.x * 0 == 0);
		assert(p.y * 0 == 0);
		assert(p.z * 0 == 0);

		pp2T.m00 = p.x * p2.x;
		pp2T.m01 = p.x * p2.y;
		pp2T.m02 = p.x * p2.z;
		pp2T.m10 = p.y * p2.x;
		pp2T.m11 = p.y * p2.y;
		pp2T.m12 = p.y * p2.z;
		pp2T.m20 = p.z * p2.x;
		pp2T.m21 = p.z * p2.y;
		pp2T.m22 = p.z * p2.z;

	}

	private static ArrayList<Float> initZero(int size) {
		ArrayList<Float> list = new ArrayList<Float>();
		for (int i = 0; i < size; i++) {
			list.add(0F);
		}
		return list;
	}

	private void scaleLastColumn(Matrix3f matrix, float scale) {
		matrix.m02 *= scale;
		matrix.m12 *= scale;
		matrix.m22 *= scale;
	}

	CSRMatrix computeI_constr(float scale) {
		int n = hs_original.getVertices().size();
		CSRMatrix I_constr = new CSRMatrix(n, n);

		HashSet<Integer> constr = new HashSet<Integer>();
		constr.addAll(keepFixed);
		constr.addAll(deform);

		for (int i : constr) {
			I_constr.set(i, i, scale);
		}

		return I_constr;
	}
}
