package assignment6;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;

import org.junit.Before;
import org.junit.Test;

import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;
import assignment4.generatedMeshes.Cylinder2;

public class RAPS_Test {

	private RAPS_modelling raps;
	private HalfEdgeStructure hs;

	@Before
	public void setUp() {

		WireframeMesh m = new Cylinder2(0.3f, 2f).result;
		this.hs = new HalfEdgeStructure();

		try {
			hs.init(m);
		} catch (MeshNotOrientedException | DanglingTriangleException e) {
			e.printStackTrace();
		}

		// Select one boundary of the cylinder
		HashSet<Integer> boundary1 = collectBoundary(hs, 3, new Constraint() {
			public boolean isEligible(Vertex v) {
				return v.getPos().x < 0.5f;
			}
		});

		// Select the second boundary of the cylinder
		HashSet<Integer> boundary2 = collectBoundary(hs, 3, new Constraint() {
			public boolean isEligible(Vertex v) {
				return v.getPos().x > 0;
			}
		});

		this.raps = new RAPS_modelling(hs);
		this.raps.keep(boundary1);
		this.raps.target(boundary2);
	}

	@Test
	public void test1() {
		/*
		 * If all rotations are set to the identity and the original, undeformed
		 * positions p' = p are used, the equation L_cotan_unnorml * p' = b must
		 * hold.
		 */
		ArrayList<Float>[] p_prime = new ArrayList[3];
		p_prime[0] = new ArrayList<Float>();
		p_prime[1] = new ArrayList<Float>();
		p_prime[2] = new ArrayList<Float>();

		HalfEdgeStructure original = raps.getOriginalCopy();
		for (int i = 0; i < original.getVertices().size(); i++) {
			Vertex vertex = original.getVertices().get(i);
			p_prime[0].add(vertex.getPos().x);
			p_prime[1].add(vertex.getPos().y);
			p_prime[2].add(vertex.getPos().z);
		}

		ArrayList<Float>[] result = new ArrayList[3];
		result[0] = new ArrayList<Float>();
		result[1] = new ArrayList<Float>();
		result[2] = new ArrayList<Float>();

		raps.L_cotan.mult(p_prime, result);

		raps.compute_b();

		float numericalError = 0.0001F;

		for (int i = 0; i < original.getVertices().size(); i++) {
			assertEquals(raps.b[0].get(i), result[0].get(i), numericalError);
			assertEquals(raps.b[1].get(i), result[1].get(i), numericalError);
			assertEquals(raps.b[2].get(i), result[2].get(i), numericalError);
		}
	}

	@Test
	public void testUndeformedMeshIdentityRotations() {
		/*
		 * For a mesh that has not been deformed, the identity rotation should
		 * be found for every vertex.
		 */
		raps.optimalRotations();

		Matrix3f id = new Matrix3f();
		id.setIdentity();

		float numericalError = 0.00001F;

		for (Matrix3f rot : raps.rotations) {
			// Check if rot equals the identity up to a small numerical error.
			assertTrue(rot.epsilonEquals(id, numericalError));
		}
	}

	@Test
	public void testRotateTranslateMesh() {
		/*
		 * Rotations and translations of the mesh without any additional
		 * deformations. The applied rotation should be found for every vertex.
		 */
		Vector3f translation = new Vector3f(-2, 1, 0.5F);
		Vector3f rotAxis = new Vector3f(-1, 1, 5);
		float rotAngle = (float) Math.PI / 3;

		Matrix4f transformation = new Matrix4f();
		transformation.setIdentity();
		transformation.setTranslation(translation);
		AxisAngle4f meshRot = new AxisAngle4f(rotAxis, rotAngle);
		transformation.setRotation(meshRot);

		Matrix3f meshRotation = new Matrix3f();
		transformation.getRotationScale(meshRotation);

		for (Vertex vertex : hs.getVertices()) {
			transformation.transform(vertex.getPos());
		}

		raps.compute_b();
		raps.optimalRotations();

		float numericalError = 0.00001F;

		for (Vertex v : raps.getOriginalCopy().getVertices()) {
			int i = v.index;
			if (v.isOnBorder()) {
				continue;
			}
			Matrix3f rot = raps.rotations.get(i);
			assertTrue(rot.epsilonEquals(meshRotation, numericalError));
		}
	}

	@Test
	public void testLaplacianMatrices() {

		assertRowSumsZero(raps.L_cotan);

		/*
		 * Test if laplacian has zero rows for border vertices
		 */
		raps.compute_b();

		for (Vertex vertex : hs.getVertices()) {
			if (vertex.isOnBorder()) {

				ArrayList<col_val> row = raps.L_cotan.getRow(vertex.index);

				for (col_val e : row) {
					assertTrue(0 == e.val);
				}

				assertTrue(0 == raps.b[0].get(vertex.index));
				assertTrue(0 == raps.b[1].get(vertex.index));
				assertTrue(0 == raps.b[2].get(vertex.index));
			}
		}

		/*
		 * Test that L_deform is symmetric
		 */
		raps.updateL();
		assertSymmetricMatrix(raps.L_deform);
	}

	private void assertSymmetricMatrix(CSRMatrix matrix) {
		assertEquals(matrix.nRows(), matrix.nCols());

		for (int i = 0; i < matrix.nRows(); i++) {
			for (int j = 0; j < matrix.nCols(); j++) {
				assertEquals(matrix.get(i, j), matrix.get(j, i), 0.00001F);
			}
		}
	}

	private void assertRowSumsZero(CSRMatrix matrix) {
		/*
		 * Row sums should equal zero
		 */
		for (int i = 0; i < matrix.nRows(); i++) {
			ArrayList<col_val> row = matrix.getRow(i);
			float sum = 0;
			for (col_val element : row) {
				sum += element.val;
			}
			assertEquals(0, sum, 0.000001F);
		}
	}

	private static HashSet<Integer> collectBoundary(HalfEdgeStructure hs, int dist, Constraint c) {
		HashSet<Integer> has_jm1_dist = new HashSet<>();
		for (Vertex v : hs.getVertices()) {
			if (v.isOnBorder() && c.isEligible(v)) {
				has_jm1_dist.add(v.index);
			}
		}

		Vertex temp;
		HashSet<Integer> has_j_dist = new HashSet<>();
		for (int j = 0; j < dist; j++) {
			for (Vertex v : hs.getVertices()) {
				Iterator<Vertex> it = v.iteratorVV();
				while (it.hasNext()) {
					temp = it.next();
					if (has_jm1_dist.contains(temp.index)) {
						has_j_dist.add(v.index);
					}
				}
			}

			HashSet<Integer> tmp = has_jm1_dist;
			has_jm1_dist = has_j_dist;
			has_j_dist = tmp;

		}
		return has_jm1_dist;
	}

}
