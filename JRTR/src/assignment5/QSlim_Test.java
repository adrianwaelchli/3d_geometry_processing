package assignment5;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;
import openGL.objects.Transformation;

/**
 * @author Adrian Waelchli
 */
public class QSlim_Test {

	private HalfEdgeStructure hs;

	@Before
	public void setUp() {
		try {
			WireframeMesh m = ObjReader.read("objs/dragon3.obj", false);
			hs = new HalfEdgeStructure();
			hs.init(m);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testDegeneratedTriangles() {
		QSlim qslim = new QSlim(hs);

		for (Vertex vertex : hs.getVertices()) {
			Transformation t = qslim.getErrorQuadric(vertex);
			assertFalse(containsNAN(t));
			assertFalse(containsINF(t));
		}
	}
	
	@Test
	public void testCostZeroAtVertex(){
		QSlim qslim = new QSlim(hs);

		for (Vertex vertex : hs.getVertices()) {
			float cost = qslim.getCost(vertex, vertex.getPos());
			assertEquals(0, cost, 0.0001);
		}
	}

	private boolean containsNAN(Transformation t) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++)

				if (Float.isNaN(t.getElement(i, j))) {
					return true;
				}
		}
		return false;
	}

	private boolean containsINF(Transformation t) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++)

				if (Float.isInfinite(t.getElement(i, j))) {
					return true;
				}
		}
		return false;
	}

}
