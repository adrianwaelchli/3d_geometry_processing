package assignment5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;

import javax.vecmath.Point3f;
import javax.vecmath.Point4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import meshes.Face;
import meshes.HalfEdge;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import openGL.objects.Transformation;

/**
 * @author Adrian Waelchli
 */
public class QSlim {

	private final HalfEdgeStructure hs;
	private HalfEdgeCollapse hsCollapse;

	private HashMap<Vertex, Transformation> errorQuadrics;
	private PriorityQueue<PotentialCollapse> queue;
	private HashMap<HalfEdge, PotentialCollapse> undeletedCollapses;

	public QSlim(HalfEdgeStructure halfedgeStructure) {
		this.hs = halfedgeStructure;
		init();
	}

	private void init() {
		errorQuadrics = new HashMap<Vertex, Transformation>();
		queue = new PriorityQueue<PotentialCollapse>();
		undeletedCollapses = new HashMap<HalfEdge, PotentialCollapse>();

		for (Vertex vertex : hs.getVertices()) {
			Transformation quadric = new Transformation();
			Point3f p0 = new Point3f(vertex.getPos());

			Iterator<Face> faceIterator = vertex.iteratorVF();
			while (faceIterator.hasNext()) {
				Face face = faceIterator.next();
				if (!face.isDegenerated()) {
					Vector3f n = face.normal();
					Vector4f plane = new Vector4f(n);
					plane.w = -n.dot(new Vector3f(p0));
					quadric.add(compute_ppT(plane));
				}
			}

			errorQuadrics.put(vertex, quadric);
		}

		for (HalfEdge edge : hs.getHalfEdges()) {
			PotentialCollapse collapse = new PotentialCollapse();
			collapse.edge = edge;
			collapse.targetPosition = findOptimizedPosition(edge);
			collapse.cost = getCost(edge, collapse.targetPosition);
			queue.add(collapse);
			undeletedCollapses.put(edge, collapse);
		}
	}

	public Transformation getErrorQuadric(Vertex vertex) {
		return this.errorQuadrics.get(vertex);
	}

	public Transformation getErrorQuadric(HalfEdge edge) {
		Transformation qu = getErrorQuadric(edge.start());
		Transformation qv = getErrorQuadric(edge.end());
		Transformation q = new Transformation(qu);
		q.add(qv);
		return q;
	}

	public float getCost(Transformation q, Point3f point) {
		Point4f qp = new Point4f(point);
		q.transform(qp);
		Vector4f v1 = new Vector4f(qp);
		Vector4f v2 = new Vector4f(new Point4f(point));
		return v1.dot(v2);
	}

	public float getCost(Vertex vertex, Point3f point) {
		Transformation q = getErrorQuadric(vertex);
		return getCost(q, point);
	}

	public float getCost(HalfEdge edge) {
		Transformation q = getErrorQuadric(edge);
		return getCost(q, findOptimizedPosition(edge));
	}

	public float getCost(HalfEdge edge, Point3f point) {
		return getCost(getErrorQuadric(edge), point);
	}

	/**
	 * The actual QSlim algorithm: Collapse edges until the target number of
	 * vertices is reached.
	 */
	public void simplify(int target) {

		hsCollapse = new HalfEdgeCollapse(hs);

		while (hs.getVertices().size() - hsCollapse.deadVertices.size() > target) {
			collapseEdge();
		}

		hsCollapse.finish();
	}

	/**
	 * Collapse the next cheapest eligible edge. This method can be called until
	 * some target number of vertices is reached.
	 */
	public void collapseEdge() {

		PotentialCollapse potentialCollapse = queue.poll();
		while (potentialCollapse.isDeleted) {
			// Ignore deleted collapses.
			potentialCollapse = queue.poll();
		}

		HalfEdge potentialEdge = potentialCollapse.edge;
		Point3f newPos = potentialCollapse.targetPosition;

		/*
		 * Edge is not collapsable or results in mesh inversion. Cost is
		 * increased.
		 */
		if (!HalfEdgeCollapse.isEdgeCollapsable(potentialEdge) || hsCollapse.isCollapseMeshInv(potentialEdge, newPos)) {
			PotentialCollapse newPotentialCollapse = new PotentialCollapse();
			newPotentialCollapse.edge = potentialEdge;
			newPotentialCollapse.cost = (potentialCollapse.cost + 0.1F) * 10;
			newPotentialCollapse.targetPosition = newPos;
			queue.offer(newPotentialCollapse);
			undeletedCollapses.put(potentialEdge, newPotentialCollapse);
			return;
		}

		ArrayList<HalfEdge> obsolete = hsCollapse.collapseEdge(potentialEdge, newPos);

		/*
		 * Mark potential collapses as deleted for half-edges that do not exist
		 * anymore.
		 */
		for (HalfEdge deadEdge : obsolete) {
			PotentialCollapse removed = undeletedCollapses.remove(deadEdge);
			removed.isDeleted = true;
		}

		/*
		 * Update error quadric for the end vertex of the half-edge.
		 */
		Transformation q = getErrorQuadric(potentialEdge);
		errorQuadrics.put(potentialEdge.end(), q);

		/*
		 * Update error quadrics for the neighboring half-edges.
		 */
		Iterator<HalfEdge> iterator = potentialEdge.end().iteratorVE();
		while (iterator.hasNext()) {
			HalfEdge adjacentEdge = iterator.next();
			/*
			 * Mark out-dated entry in priority queue as deleted and add new
			 * entry for each edge with new cost.
			 */
			PotentialCollapse c = undeletedCollapses.get(adjacentEdge);
			c.isDeleted = true;
			PotentialCollapse updated = new PotentialCollapse();
			updated.edge = adjacentEdge;
			updated.targetPosition = c.targetPosition;
			updated.cost = getCost(adjacentEdge, c.targetPosition);
			queue.offer(updated);
			undeletedCollapses.put(adjacentEdge, updated);
		}
	}

	private Point3f findOptimizedPosition(HalfEdge edge) {
		Transformation q_ = new Transformation(getErrorQuadric(edge));
		q_.setRow(3, new Vector4f(0, 0, 0, 1));

		if (q_.determinant() == 0) {
			return edge.middle();
		}

		q_.invert();
		Point4f position4f = new Point4f(0, 0, 0, 1);
		q_.transform(position4f);
		return new Point3f(position4f.x, position4f.y, position4f.z);
	}

	/**
	 * Computes the outer product of p and transpose(p).
	 */
	private Transformation compute_ppT(Vector4f p) {
		Transformation ppT = new Transformation();

		assert(p.x * 0 == 0);
		assert(p.y * 0 == 0);
		assert(p.z * 0 == 0);
		assert(p.w * 0 == 0);

		ppT.m00 = p.x * p.x;
		ppT.m01 = p.x * p.y;
		ppT.m02 = p.x * p.z;
		ppT.m03 = p.x * p.w;
		ppT.m10 = p.y * p.x;
		ppT.m11 = p.y * p.y;
		ppT.m12 = p.y * p.z;
		ppT.m13 = p.y * p.w;
		ppT.m20 = p.z * p.x;
		ppT.m21 = p.z * p.y;
		ppT.m22 = p.z * p.z;
		ppT.m23 = p.z * p.w;
		ppT.m30 = p.w * p.x;
		ppT.m31 = p.w * p.y;
		ppT.m32 = p.w * p.z;
		ppT.m33 = p.w * p.w;

		return ppT;
	}

	/**
	 * Represent a potential collapse
	 * 
	 * @author Adrian Waelchli
	 *
	 */
	protected class PotentialCollapse implements Comparable<PotentialCollapse> {

		HalfEdge edge;
		Point3f targetPosition;
		float cost;
		boolean isDeleted = false;

		@Override
		public int compareTo(PotentialCollapse other) {
			if (cost == other.cost) {
				return 0;
			} else if (cost < other.cost) {
				return -1;
			} else {
				return 1;
			}
		}
	}

}
