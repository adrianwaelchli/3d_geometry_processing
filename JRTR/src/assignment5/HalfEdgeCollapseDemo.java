package assignment5;

import java.io.IOException;

import glWrapper.GLHalfedgeStructure;
import meshes.HalfEdge;
import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;
import openGL.MyDisplay;

public class HalfEdgeCollapseDemo {

	public static void main(String[] args) {
		randomCollapseDemo("objs/bunny5k.obj", 2000);
		epsilonCollapseDemo("objs/bunny5k.obj", 0.1F);
	}
	
	public static void epsilonCollapseDemo(String object, float epsilon){
		HalfEdgeStructure hs = null;
		try {
			WireframeMesh wf = ObjReader.read(object, true);
			hs = new HalfEdgeStructure();
			hs.init(wf);
		} catch (IOException | MeshNotOrientedException | DanglingTriangleException e) {
			e.printStackTrace();
		}
		
		MyDisplay display = new MyDisplay();
		GLHalfedgeStructure glHsBefore = new GLHalfedgeStructure(hs);
		glHsBefore.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glHsBefore);
		
		HalfEdgeCollapse collapse = new HalfEdgeCollapse(hs);
		
		int numCollapses = collapse.epsilonCollapse(epsilon);
		System.out.printf("%d edges collapsed.\n", numCollapses);
		
		GLHalfedgeStructure glHsAfter = new GLHalfedgeStructure(hs);
		glHsAfter.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glHsAfter);
	}

	public static void randomCollapseDemo(String object, int numCollapses) {
		/*
		 * Randomly collapse some edges
		 */
		HalfEdgeStructure hs = null;
		try {
			WireframeMesh wf = ObjReader.read(object, true);
			hs = new HalfEdgeStructure();
			hs.init(wf);
		} catch (IOException | MeshNotOrientedException | DanglingTriangleException e) {
			e.printStackTrace();
		}

		HalfEdgeCollapse collapse = new HalfEdgeCollapse(hs);

		for (int i = 0; i < numCollapses; i++) {
			int size = hs.getHalfEdges().size();
			int edgeIndex = (int) Math.floor(Math.random() * size);

			HalfEdge candidate = hs.getHalfEdges().get(edgeIndex);

			if (HalfEdgeCollapse.isEdgeCollapsable(candidate)) {
				collapse.collapseEdgeAndDelete(candidate);
			} else {
				i--;
			}
		}

		MyDisplay display = new MyDisplay();

		GLHalfedgeStructure glHs = new GLHalfedgeStructure(hs);
		glHs.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");

		display.addToDisplay(glHs);
	}

}
