package assignment5;

import java.io.IOException;

import glWrapper.GLHalfedgeStructure;
import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;
import openGL.MyDisplay;

public class QSlimDemo {

	public static void main(String[] args) {
		qSlimDemo("objs/dragon.obj", 1500);
	}
	
	public static void qSlimDemo(String object, int targetCollapses){
		
		HalfEdgeStructure hs = null;
		try {
			WireframeMesh wf = ObjReader.read(object, true);
			hs = new HalfEdgeStructure();
			hs.init(wf);
		} catch (IOException | MeshNotOrientedException | DanglingTriangleException e) {
			e.printStackTrace();
		}
		
		System.out.printf("Number of vertices: %d\n", hs.getVertices().size());
		System.out.printf("Number of faces: %d\n", hs.getFaces().size());
		
		MyDisplay display = new MyDisplay();
		GLHalfedgeStructure glHsBefore = new GLHalfedgeStructure(hs);
		glHsBefore.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glHsBefore);
		
		long start = System.currentTimeMillis();
		
		QSlim qslim = new QSlim(hs);
		qslim.simplify(targetCollapses);
		
		long stop = System.currentTimeMillis();
		System.out.printf("Elapsed time: %d seconds.\n", (long) Math.ceil((stop - start) / 1000.0));
		
		GLHalfedgeStructure glHsAfter = new GLHalfedgeStructure(hs);
		glHsAfter.configurePreferredShader("shaders/trimesh_flat.vert", "shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		display.addToDisplay(glHsAfter);
		
	}

}
