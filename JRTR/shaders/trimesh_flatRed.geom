#version 150

uniform mat4 projection; 
uniform mat4 modelview;

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec4 position_g[];

flat out vec3 normal_g;

void main()
{		
	normal_g = normalize(cross(position_g[2].xyz - position_g[0].xyz,
					position_g[1].xyz - position_g[0].xyz));
	
	for(int i=0; i<3; i++)
	{
		gl_Position = projection * position_g[i];
		gl_PrimitiveID = gl_PrimitiveIDIn;
		EmitVertex();
	}
	EndPrimitive();
	
	
	
}
