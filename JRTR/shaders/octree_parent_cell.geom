#version 150

uniform mat4 projection; 
uniform mat4 modelview;

layout(points) in;
layout(line_strip, max_vertices = 2) out;

in vec4 position_g[];
in vec4 parentPosition_g[];

void main()
{		
	gl_PrimitiveID = gl_PrimitiveIDIn;
	
	gl_Position = projection * modelview * position_g[0];
	EmitVertex();
	
	gl_Position = projection * modelview * parentPosition_g[0];
	EmitVertex();
	
	EndPrimitive();
}
