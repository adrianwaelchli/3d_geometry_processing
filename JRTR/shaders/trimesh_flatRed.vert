#version 150

uniform mat4 projection; 
uniform mat4 modelview;

in vec4 position;

out vec4 position_g;

void main()
{
	position_g = modelview * position;
}
