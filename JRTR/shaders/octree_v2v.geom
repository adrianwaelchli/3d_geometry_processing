#version 150

uniform mat4 projection; 
uniform mat4 modelview;

layout(points) in;
layout(line_strip, max_vertices = 12) out;

in vec4 position_g[];

in vec4 position_v2v_100_g[];
in vec4 position_v2v_010_g[];
in vec4 position_v2v_001_g[];
in vec4 position_v2v_minus_100_g[];
in vec4 position_v2v_minus_010_g[];
in vec4 position_v2v_minus_001_g[];

void main()
{		
	gl_PrimitiveID = gl_PrimitiveIDIn;
	
	float length = 0.25;
	vec4 p = position_g[0];
	
	gl_Position = projection * modelview * p;
	EmitVertex();
	gl_Position = projection * modelview * (p + (position_v2v_100_g[0] - p) * length);
	EmitVertex();
	EndPrimitive();
	
	gl_Position = projection * modelview * p;
	EmitVertex();
	gl_Position = projection * modelview * (p + (position_v2v_010_g[0] - p) * length);
	EmitVertex();
	EndPrimitive();
	
	gl_Position = projection * modelview * p;
	EmitVertex();
	gl_Position = projection * modelview * (p + (position_v2v_001_g[0] - p) * length);
	EmitVertex();
	EndPrimitive();
	
	gl_Position = projection * modelview * p;
	EmitVertex();
	gl_Position = projection * modelview * (p + (position_v2v_minus_100_g[0] - p) * length);
	EmitVertex();
	EndPrimitive();
	
	gl_Position = projection * modelview * p;
	EmitVertex();
	gl_Position = projection * modelview * (p + (position_v2v_minus_010_g[0] - p) * length);
	EmitVertex();
	EndPrimitive();
	
	gl_Position = projection * modelview * p;
	EmitVertex();
	gl_Position = projection * modelview * (p + (position_v2v_minus_001_g[0] - p) * length);
	EmitVertex();
	EndPrimitive();
}
