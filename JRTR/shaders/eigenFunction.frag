#version 150

in float frag_eigenFunction;
out vec4 out_color;

void main()
{		
	float v = frag_eigenFunction;
	
	float r = min(2 * max(v, 0.1), 0.8);
	float b = min(2 * max(1 - v, 0.1), 0.8);
	float g = min(r, b);
	
	out_color = vec4(r, g, b, 1);	
}
