#version 150

in float frag_meanCurvature;
out vec4 out_color;

void main()
{		
	float v = log(1 + frag_meanCurvature / 10);
	
	float r = v >= 1 ? v - 1 : 0;
	float g = abs(v - 1) < 1 ? 1 - abs(1 - v) : 0;
	float b = v < 1 ? 1 - v : 0;
	
	out_color = vec4(r, g, b, 1);	
}
