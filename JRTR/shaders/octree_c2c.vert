#version 150

uniform mat4 projection; 
uniform mat4 modelview;

in vec4 position;

in vec4 position_c2c_100;
in vec4 position_c2c_010;
in vec4 position_c2c_001;
in vec4 position_c2c_minus_100;
in vec4 position_c2c_minus_010;
in vec4 position_c2c_minus_001;

out vec4 position_c2c_100_g;
out vec4 position_c2c_010_g;
out vec4 position_c2c_001_g;
out vec4 position_c2c_minus_100_g;
out vec4 position_c2c_minus_010_g;
out vec4 position_c2c_minus_001_g;

out vec4 position_g;

void main()
{
	position_g = position;
	
	position_c2c_100_g = position_c2c_100;
	position_c2c_010_g = position_c2c_010;
	position_c2c_001_g = position_c2c_001;
	position_c2c_minus_100_g = position_c2c_minus_100;
	position_c2c_minus_010_g = position_c2c_minus_010;
	position_c2c_minus_001_g = position_c2c_minus_001;
}
