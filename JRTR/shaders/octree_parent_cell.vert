#version 150

uniform mat4 projection; 
uniform mat4 modelview;

in vec4 position;
in vec4 parentPosition;

out vec4 position_g;
out vec4 parentPosition_g;

void main()
{
	position_g = position;
	parentPosition_g = parentPosition;
}
