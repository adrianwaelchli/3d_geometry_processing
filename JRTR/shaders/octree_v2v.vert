#version 150

uniform mat4 projection; 
uniform mat4 modelview;

in vec4 position;

in vec4 position_v2v_100;
in vec4 position_v2v_010;
in vec4 position_v2v_001;
in vec4 position_v2v_minus_100;
in vec4 position_v2v_minus_010;
in vec4 position_v2v_minus_001;

out vec4 position_v2v_100_g;
out vec4 position_v2v_010_g;
out vec4 position_v2v_001_g;
out vec4 position_v2v_minus_100_g;
out vec4 position_v2v_minus_010_g;
out vec4 position_v2v_minus_001_g;

out vec4 position_g;

void main()
{
	position_g = position;
	
	position_v2v_100_g = position_v2v_100;
	position_v2v_010_g = position_v2v_010;
	position_v2v_001_g = position_v2v_001;
	position_v2v_minus_100_g = position_v2v_minus_100;
	position_v2v_minus_010_g = position_v2v_minus_010;
	position_v2v_minus_001_g = position_v2v_minus_001;
}
