#version 150

uniform mat4 projection; 
uniform mat4 modelview;

in vec3 position;
in vec3 curvatureNormal;

out Vertex 
{
	vec4 position;
	vec4 normal;
	vec4 color;
	float curvature;
	
} vertex;

void main()
{
	vertex.position = vec4(position, 1.0);
	vertex.normal = vec4(normalize(curvatureNormal), 0.0);
	vertex.color = vec4(1.0, 0.0, 0.0, 1.0);
	vertex.curvature = length(curvatureNormal);
}
