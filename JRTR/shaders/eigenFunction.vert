#version 150

uniform mat4 projection; 
uniform mat4 modelview;

in vec4 position;
in float eigenFunction;

out float frag_eigenFunction;

void main()
{
	gl_Position = projection * modelview * position;
	frag_eigenFunction = eigenFunction;
}
