Format of Labels:

Each row has the format:
<index> <v1> <v2> <v3> <alpha> <beta> <gamma>

Where:
<index> := The landmark index (see landmarks_index.png)
<v1>,...,<v3> := Vertex index (i.e. v.index)
<alpha>, <beta>, <gamma> := Barycentric coordinates (i.e. landmarkPosition = alpha*v1+beta*v2+gamma*v3)
